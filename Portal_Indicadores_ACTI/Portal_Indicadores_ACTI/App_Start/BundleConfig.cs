﻿using System.Web;
using System.Web.Optimization;

namespace Portal_Indicadores_ACTI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/jquery.js",
                        "~/Scripts/jquery-ui-1.11.4.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/metisMenu/metisMenu.js",
                        "~/Scripts/scroll-top.js",
                        "~/Scripts/plantilla.js"));

            bundles.Add(new ScriptBundle("~/bundles/scriptsIndicadores").Include(
                        "~/Scripts/Highcharts-4.0.1/js/highcharts.js",
                        "~/Scripts/Highcharts-4.0.1/js/modules/data.js",
                        "~/Scripts/Highcharts-4.0.1/js/modules/drilldown.js",
                        "~/Scripts/Highcharts-4.0.1/js/modules/exporting.js",
                        "~/Scripts/Highcharts-4.0.1/js/themes/grid.js",
                        "~/Scripts/pivotTable/dist/pivot.js",
                        "~/Scripts/jquery-ui-touch-punch.js"));

            bundles.Add(new ScriptBundle("~/bundles/tipuesearch").Include(
                        "~/Scripts/tipuesearch/tipuesearch_set.js",
                        "~/Scripts/tipuesearch/tipuesearch_content.js",
                        "~/Scripts/tipuesearch/tipuesearch.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/Bootstrap/bootstrap.css",
                      "~/Content/metisMenu/metisMenu.css",
                      "~/Content/master_page.css",
                      "~/Content/pageStyle.css",
                      "~/Scripts/tipuesearch/tipuesearch.css",
                      "~/font-awesome/css/font-awesome.css"));

            bundles.Add(new StyleBundle("~/tipuesearch/css").Include(
                      "~/Scripts/tipuesearch/tipuesearch.css"));
        }
    }
}
