﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Portal_Indicadores_ACTI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Inicio", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Descarga",
                url: "{controller}/{action}/{id}/{desde}/{hasta}/{tipoReporte}/{fileName}",
                defaults: new
                {
                    controller = "Graduados",
                    action = "Index",
                    id = UrlParameter.Optional,
                    desde = UrlParameter.Optional,
                    hasta = UrlParameter.Optional,
                    tipoReporte = UrlParameter.Optional,
                    fileName = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "Descargar",
                url: "{controller}/{action}/{id}/{desde}/{hasta}/{tipoReporte}/{acti}/{fileName}",
                defaults: new
                {
                    controller = "Productos",
                    action = "Index",
                    id = UrlParameter.Optional,
                    desde = UrlParameter.Optional,
                    hasta = UrlParameter.Optional,
                    tipoReporte = UrlParameter.Optional,
                    acti = UrlParameter.Optional,
                    fileName = UrlParameter.Optional
                }
            );
        }


    }
}
