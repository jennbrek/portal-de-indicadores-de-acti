﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class EquiposLaboratorioController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 88675309 // Usa este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /EquiposLaboratorio/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /EquiposLaboratorio/getYears
        //lista los años de registros de EquiposLaboratorios
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_EquiposLaboratorios
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EquiposLaboratorio/Listar
        //lista todos los EquiposLaboratorio
        public JsonResult Listar()
        {
            var query = from r in db.Lista_EquiposLaboratorios
                        select new
                        {
                            Año = r.año,
                            Periodo = r.Periodo,
                            Área_OCDE = r.Area,
                            Disciplina_OCDE = r.Disciplina,
                            Subárea_OCDE = r.Subarea,
                            ACTI_N1 = r.ACTI_N1,
                            ACTI_N2 = r.ACTI_N2,
                            ACTI_N3 = r.ACTI_N3,
                            Complejidad_Tecnológica = r.Complejidad,
                            Tiempo_Servicio = r.rango,
                            Cantidad = r.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EquiposLaboratorio/ListarxYear/tabla/desde/hasta
        //lista todos los EquiposLaboratorio segun el tipo de tabla, el año (con semestre) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from r in db.Database
                    .SqlQuery<equiposLaboratorios_dimension_años_Result>("equiposLaboratorios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Subárea_OCDE = r.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from r in db.Database
                    .SqlQuery<equiposLaboratorios_dimension_años_Result>("equiposLaboratorios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Disciplina_OCDE = r.nombre
                            };
                    break;
                case "4"://acti primer nivel
                    query = from r in db.Database
                    .SqlQuery<equiposLaboratorios_dimension_años_Result>("equiposLaboratorios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N1 = r.nombre
                            };
                    break;
                case "5"://acti segundo nivel
                    query = from r in db.Database
                    .SqlQuery<equiposLaboratorios_dimension_años_Result>("equiposLaboratorios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N2 = r.nombre
                            };
                    break;
                case "6"://acti tercer nivel
                    query = from r in db.Database
                    .SqlQuery<equiposLaboratorios_dimension_años_Result>("equiposLaboratorios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N3 = r.nombre
                            };
                    break;
                case "7"://complejidad tecnológica
                    query = from r in db.Database
                    .SqlQuery<equiposLaboratorios_dimension_años_Result>("equiposLaboratorios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Complejidad_Tecnológica = r.nombre
                            };
                    break;
                case "8"://Años servicios
                    query = from r in db.Database
                    .SqlQuery<equiposLaboratorios_dimension_años_Result>("equiposLaboratorios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Tiempo_Servicio = r.nombre,
                            };
                    break;
                default://Área OCDE
                    query = from r in db.Database
                    .SqlQuery<equiposLaboratorios_dimension_años_Result>("equiposLaboratorios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Área_OCDE = r.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EquiposLaboratorio/GraficoxYear/tabla/desde/hasta
        //lista todos los EquiposLaboratorio segun el tipo de tabla, el año (sin semestre) desde y hasta.
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from r in db.Database
                    .SqlQuery<equiposLaboratorios_dimension_años_Result>("equiposLaboratorios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby r.año, r.nombre
                        select new
                        {
                            r.año,
                            r.total,
                            r.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EquiposLaboratorio/EquiposLaboratoriosOCDEACTI/yearSearch/catOCDE/catACTI
        //Lista los EquiposLaboratorio en un año por categoria ocde y nivel acti
        public JsonResult EquiposLaboratoriosOCDEACTI(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from r in db.Database
                .SqlQuery<equiposLaboratorios_ocde_acti_Result>("equiposLaboratorios_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.acti
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EquiposLaboratorio/EquiposLaboratoriosComplejidadACTI/yearSearch/catACTI
        //Lista las EquiposLaboratorio para ACTI en un año por nivel acti y complejidad tecnológica
        public JsonResult EquiposLaboratoriosComplejidadACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<equiposLaboratorios_acti_complejidad_Result>("EquiposLaboratorios_acti_complejidad @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.complejidad
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.complejidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EquiposLaboratorio/EquiposLaboratoriosServiciosACTI/yearSearch/catACTI
        //Lista las EquiposLaboratorio para ACTI en un año por nivel acti y años de servicios
        public JsonResult EquiposLaboratoriosServiciosACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<equiposLaboratorios_acti_rangoServicio_Result>("EquiposLaboratorios_acti_rangoServicio @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.rango
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.rango
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EquiposLaboratorio/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de EquiposLaboratorio por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/EquiposLaboratorio"), "EquiposLaboratorio_área_ocde.rdlc");
                    break;
                case "2": //reporte de EquiposLaboratorio por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/EquiposLaboratorio"), "EquiposLaboratorio_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de EquiposLaboratorio por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/EquiposLaboratorio"), "EquiposLaboratorio_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de EquiposLaboratorio por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/EquiposLaboratorio"), "EquiposLaboratorio_acti_n1.rdlc");
                    break;
                case "5": //reporte de EquiposLaboratorio por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/EquiposLaboratorio"), "EquiposLaboratorio_acti_n2.rdlc");
                    break;
                case "6": //reporte de EquiposLaboratorio por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/EquiposLaboratorio"), "EquiposLaboratorio_acti_n3.rdlc");
                    break;
                case "7": //reporte de EquiposLaboratorio por complejidad tecnológica
                    path = Path.Combine(Server.MapPath("~/Reportes/EquiposLaboratorio"), "EquiposLaboratorio_complejidad_tecnológica.rdlc");
                    break;
                case "8": //reporte de EquiposLaboratorio por años de servicios
                    path = Path.Combine(Server.MapPath("~/Reportes/EquiposLaboratorio"), "EquiposLaboratorio_tiempo_servicio.rdlc");
                    break;
                default: //reporte de EquiposLaboratorio completo
                    path = Path.Combine(Server.MapPath("~/Reportes/EquiposLaboratorio"), "EquiposLaboratorio.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from r in db.Lista_EquiposLaboratorios
                        select new
                        {
                            Año = r.año,
                            r.Periodo,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Complejidad,
                            Rango = r.rango,
                            r.Cantidad
                        };
            }
            else
            {
                query = from r in db.Lista_EquiposLaboratorios.ToList()
                        where
                           r.año.ToString().CompareTo(desde) >= 0 &&
                           r.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = r.año,
                            r.Periodo,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Complejidad,
                            Rango = r.rango,
                            r.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}