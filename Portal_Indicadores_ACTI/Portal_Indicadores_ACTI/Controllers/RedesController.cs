﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class RedesController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 88675309 // Usa este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /Redes/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Redes/getYears
        //lista los años de registros de redes
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_Redes
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Redes/Listar
        //lista todas las redes
        public JsonResult Listar()
        {
            var query = from r in db.Lista_Redes
                        select new
                        {
                            Año = r.año,
                            Área_OCDE = r.Area,
                            Disciplina_OCDE = r.Disciplina,
                            Subárea_OCDE = r.Subarea,
                            ACTI_N1 = r.ACTI_N1,
                            ACTI_N2 = r.ACTI_N2,
                            ACTI_N3 = r.ACTI_N3,
                            Clase_CIIU = r.Clase,
                            Grupo_CIIU = r.Grupo,
                            División_CIIU = r.Division,
                            Sección_CIIU = r.Seccion,
                            Ámbito_Geográfico = r.Ambito,
                            Resultado = r.Resultado,
                            Cantidad = r.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Redes/ListarxYear/tabla/desde/hasta
        //lista todas las redes segun el tipo de tabla, el año desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Subárea_OCDE = r.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Disciplina_OCDE = r.nombre
                            };
                    break;
                case "4"://acti primer nivel
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N1 = r.nombre
                            };
                    break;
                case "5"://acti segundo nivel
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N2 = r.nombre
                            };
                    break;
                case "6"://acti tercer nivel
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N3 = r.nombre
                            };
                    break;
                case "7"://Clase CIIU
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Clase_CIIU = r.nombre
                            };
                    break;
                case "8"://Grupo CIIU
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Grupo_CIIU = r.nombre
                            };
                    break;
                case "9"://División CIIU
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                División_CIIU = r.nombre
                            };
                    break;
                case "10"://Sección CIIU
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Sección_CIIU = r.nombre
                            };
                    break;
                case "11"://Ámbito geográfico
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Ámbito_Geográfico = r.nombre
                            };
                    break;
                case "12"://Resultado
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Resultado = r.nombre
                            };
                    break;
                default://Área OCDE
                    query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Área_OCDE = r.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Redes/GraficoxYear/tabla/desde/hasta
        //lista todas las redes segun el tipo de tabla, el año desde y hasta.
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from r in db.Database
                    .SqlQuery<redes_dimension_años_Result>("redes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby r.año, r.nombre
                        select new
                        {
                            r.año,
                            r.total,
                            r.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Redes/RedesCIIUOCDE/yearSearch/catCIIU/catOCDE
        //Lista las redes para ACTI en un año por nivel ciiu y categoria ocde
        public JsonResult RedesCIIUOCDE(string yearSearch, string catCIIU, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from r in db.Database
                .SqlQuery<redes_ciiu_ocde_Result>("redes_ciiu_ocde @year, @ciiu, @ocde", yearParameter, ciiuParameter, ocdeParameter)
                .ToList()
                        orderby r.nombre, r.ocde
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.ocde
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Redes/RedesOCDEACTI/yearSearch/catOCDE/catACTI
        //Lista las redes para ACTI en un año por categoria ocde y nivel acti
        public JsonResult RedesOCDEACTI(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from r in db.Database
                .SqlQuery<redes_ocde_acti_Result>("redes_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.acti
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Redes/RedesCIIUACTI/yearSearch/catCIIU/catACTI
        //Lista las redes para ACTI en un año por nivel ciiu y nivel acti
        public JsonResult RedesCIIUACTI(string yearSearch, string catCIIU, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<redes_ciiu_acti_Result>("redes_ciiu_acti @year, @ciiu, @acti", yearParameter, ciiuParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.acti
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Redes/RedesAmbitoGeoACTI/yearSearch/catACTI
        //Lista las redes para ACTI en un año por nivel acti y ámbito geográfico
        public JsonResult RedesAmbitoGeoACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<redes_acti_ambGeo_Result>("redes_acti_ambGeo @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.ambito
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.ambito
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Redes/RedesResultadoACTI/yearSearch/catACTI
        //Lista las redes para ACTI en un año por nivel acti y resultado del convenio
        public JsonResult RedesResultadoACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<redes_acti_resultado_Result>("redes_acti_resultado @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.resultado
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.resultado
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Redes/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de Redes por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_área_ocde.rdlc");
                    break;
                case "2": //reporte de Redes por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de Redes por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de Redes por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_acti_n1.rdlc");
                    break;
                case "5": //reporte de Redes por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_acti_n2.rdlc");
                    break;
                case "6": //reporte de Redes por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_acti_n3.rdlc");
                    break;
                case "7": //reporte de Redes por clase CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_clase_ciiu.rdlc");
                    break;
                case "8": //reporte de Redes por grupo CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_grupo_ciiu.rdlc");
                    break;
                case "9": //reporte de Redes por división CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_división_ciiu.rdlc");
                    break;
                case "10": //reporte de Redes por sección CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_sección_ciiu.rdlc");
                    break;
                case "11": //reporte de Redes por ámbito geográfico
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_ámbito_geográfico.rdlc");
                    break;
                case "12": //reporte de Redes por resultado
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes_resultado.rdlc");
                    break;
                default: //reporte de Redes completo
                    path = Path.Combine(Server.MapPath("~/Reportes/Redes"), "Redes.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from r in db.Lista_Redes
                        select new
                        {
                            Año = r.año,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Clase,
                            r.Grupo,
                            r.Division,
                            r.Seccion,
                            r.Ambito,
                            r.Resultado,
                            r.Cantidad
                        };
            }
            else
            {
                query = from r in db.Lista_Redes.ToList()
                        where
                           r.año.ToString().CompareTo(desde) >= 0 &&
                           r.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = r.año,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Clase,
                            r.Grupo,
                            r.Division,
                            r.Seccion,
                            r.Ambito,
                            r.Resultado,
                            r.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}