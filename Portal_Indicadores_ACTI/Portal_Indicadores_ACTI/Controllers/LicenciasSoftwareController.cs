﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class LicenciasSoftwareController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /LicenciasSoftware/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /LicenciasSoftware/getYears
        //lista los años de registros de LicenciasSoftware
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_LicenciasSW
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /LicenciasSoftware/Listar
        //lista todas las LicenciasSoftware
        public JsonResult Listar()
        {
            var query = from r in db.Lista_LicenciasSW
                        select new
                        {
                            Año = r.año,
                            Área_OCDE = r.Area,
                            Disciplina_OCDE = r.Disciplina,
                            Subárea_OCDE = r.Subarea,
                            ACTI_N1 = r.ACTI_N1,
                            ACTI_N2 = r.ACTI_N2,
                            ACTI_N3 = r.ACTI_N3,
                            Tipo_Software = r.TipoSW,
                            Tipo_Licencia = r.Licencia,
                            Cantidad = r.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /LicenciasSoftware/ListarxYear/tabla/desde/hasta
        //lista todas las LicenciasSoftware segun el tipo de tabla, el año desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from r in db.Database
                    .SqlQuery<licenciasSW_dimension_años_Result>("licenciasSW_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Subárea_OCDE = r.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from r in db.Database
                    .SqlQuery<licenciasSW_dimension_años_Result>("licenciasSW_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Disciplina_OCDE = r.nombre
                            };
                    break;
                case "4"://acti primer nivel
                    query = from r in db.Database
                    .SqlQuery<licenciasSW_dimension_años_Result>("licenciasSW_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N1 = r.nombre
                            };
                    break;
                case "5"://acti segundo nivel
                    query = from r in db.Database
                    .SqlQuery<licenciasSW_dimension_años_Result>("licenciasSW_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N2 = r.nombre
                            };
                    break;
                case "6"://acti tercer nivel
                    query = from r in db.Database
                    .SqlQuery<licenciasSW_dimension_años_Result>("licenciasSW_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N3 = r.nombre
                            };
                    break;
                case "7"://Tipo de software
                    query = from r in db.Database
                    .SqlQuery<licenciasSW_dimension_años_Result>("licenciasSW_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Tipo_Software = r.nombre
                            };
                    break;
                case "8"://tipo de licencia
                    query = from r in db.Database
                    .SqlQuery<licenciasSW_dimension_años_Result>("licenciasSW_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Tipo_Licencia = r.nombre
                            };
                    break;
                default://Área OCDE
                    query = from r in db.Database
                    .SqlQuery<licenciasSW_dimension_años_Result>("licenciasSW_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Área_OCDE = r.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /LicenciasSoftware/GraficoxYear/tabla/desde/hasta
        //lista todas las LicenciasSoftware segun el tipo de tabla, el año desde y hasta.
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from r in db.Database
                    .SqlQuery<licenciasSW_dimension_años_Result>("licenciasSW_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby r.año, r.nombre
                        select new
                        {
                            r.año,
                            r.total,
                            r.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /LicenciasSoftware/LicenciasSoftwareOCDEACTI/yearSearch/catOCDE/catACTI
        //Lista las LicenciasSoftware para ACTI en un año por categoria ocde y nivel acti
        public JsonResult LicenciasSoftwareOCDEACTI(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from r in db.Database
                .SqlQuery<licenciasSW_ocde_acti_Result>("licenciasSW_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.acti
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /LicenciasSoftware/LicenciasSoftwareTipoSWACTI/yearSearch/catACTI
        //Lista las LicenciasSoftware para ACTI en un año por nivel acti y tipo de software
        public JsonResult LicenciasSoftwareTipoSWACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<licenciasSW_acti_tipoSW_Result>("licenciasSW_acti_tipoSW @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.software
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.software
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /LicenciasSoftware/LicenciasSoftwareTipoLicenciaACTI/yearSearch/catACTI
        //Lista las LicenciasSoftware para ACTI en un año por nivel acti y tipo de licencia
        public JsonResult LicenciasSoftwareTipoLicenciaACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<licenciasSW_acti_tipoLicencia_Result>("licenciasSW_acti_tipoLicencia @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.licencia
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.licencia
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /LicenciasSoftware/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de LicenciasSoftware por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/LicenciasSoftware"), "LicenciasSoftware_área_ocde.rdlc");
                    break;
                case "2": //reporte de LicenciasSoftware por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/LicenciasSoftware"), "LicenciasSoftware_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de LicenciasSoftware por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/LicenciasSoftware"), "LicenciasSoftware_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de LicenciasSoftware por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/LicenciasSoftware"), "LicenciasSoftware_acti_n1.rdlc");
                    break;
                case "5": //reporte de LicenciasSoftware por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/LicenciasSoftware"), "LicenciasSoftware_acti_n2.rdlc");
                    break;
                case "6": //reporte de LicenciasSoftware por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/LicenciasSoftware"), "LicenciasSoftware_acti_n3.rdlc");
                    break;
                case "7": //reporte de LicenciasSoftware por tipo de software
                    path = Path.Combine(Server.MapPath("~/Reportes/LicenciasSoftware"), "LicenciasSoftware_tipo_software.rdlc");
                    break;
                case "8": //reporte de LicenciasSoftware por tipo de licencia
                    path = Path.Combine(Server.MapPath("~/Reportes/LicenciasSoftware"), "LicenciasSoftware_tipo_licencia.rdlc");
                    break;
                default: //reporte de LicenciasSoftware completo
                    path = Path.Combine(Server.MapPath("~/Reportes/LicenciasSoftware"), "LicenciasSoftware.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from r in db.Lista_LicenciasSW
                        select new
                        {
                            Año = r.año,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.TipoSW,
                            r.Licencia,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Cantidad
                        };
            }
            else
            {
                query = from r in db.Lista_LicenciasSW.ToList()
                        where
                           r.año.ToString().CompareTo(desde) >= 0 &&
                           r.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = r.año,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.TipoSW,
                            r.Licencia,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }

    }
}