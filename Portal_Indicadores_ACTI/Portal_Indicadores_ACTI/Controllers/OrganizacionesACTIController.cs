﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class OrganizacionesACTIController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /OrganizacionesACTI/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /OrganizacionesACTI/getYears
        //lista los años de registros de organizaciones para acti
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_OrganizacionesACTI
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /OrganizacionesACTI/Listar
        //lista todas las OrganizacionesACTI
        public JsonResult Listar()
        {
            var query = from o in db.Lista_OrganizacionesACTI
                        select new
                        {
                            Año = o.año,
                            Área_OCDE = o.Area,
                            Disciplina_OCDE = o.Disciplina,
                            Subárea_OCDE = o.Subarea,
                            ACTI_N1 = o.ACTI_N1,
                            ACTI_N2 = o.ACTI_N2,
                            ACTI_N3 = o.ACTI_N3,
                            Naturaleza_Unidad_Organizativa = o.NaturalezaOrg,
                            Tipo_Unidad_Organizativa = o.TipoOrg,
                            Cantidad = o.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /OrganizacionesACTI/ListarxYear/tabla/desde/hasta
        //lista todas las OrganizacionesACTI segun el tipo de tabla, el año desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from o in db.Database
                    .SqlQuery<organizacionesACTI_dimension_años_Result>("organizacionesACTI_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby o.año, o.nombre
                            select new
                            {
                                Año = o.año,
                                Cantidad = o.total,
                                Subárea_OCDE = o.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from o in db.Database
                    .SqlQuery<organizacionesACTI_dimension_años_Result>("organizacionesACTI_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby o.año, o.nombre
                            select new
                            {
                                Año = o.año,
                                Cantidad = o.total,
                                Disciplina_OCDE = o.nombre
                            };
                    break;
                case "4"://acti primer nivel
                    query = from o in db.Database
                    .SqlQuery<organizacionesACTI_dimension_años_Result>("organizacionesACTI_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby o.año, o.nombre
                            select new
                            {
                                Año = o.año,
                                Cantidad = o.total,
                                ACTI_N1 = o.nombre
                            };
                    break;
                case "5"://acti segundo nivel
                    query = from o in db.Database
                    .SqlQuery<organizacionesACTI_dimension_años_Result>("organizacionesACTI_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby o.año, o.nombre
                            select new
                            {
                                Año = o.año,
                                Cantidad = o.total,
                                ACTI_N2 = o.nombre
                            };
                    break;
                case "6"://acti tercer nivel
                    query = from o in db.Database
                    .SqlQuery<organizacionesACTI_dimension_años_Result>("organizacionesACTI_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby o.año, o.nombre
                            select new
                            {
                                Año = o.año,
                                Cantidad = o.total,
                                ACTI_N3 = o.nombre
                            };
                    break;
                case "7"://Naturaleza organización
                    query = from o in db.Database
                    .SqlQuery<organizacionesACTI_dimension_años_Result>("organizacionesACTI_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby o.año, o.nombre
                            select new
                            {
                                Año = o.año,
                                Cantidad = o.total,
                                Naturaleza_Unidad_Organizativa = o.nombre
                            };
                    break;
                case "8"://Tipo organización
                    query = from o in db.Database
                    .SqlQuery<organizacionesACTI_dimension_años_Result>("organizacionesACTI_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby o.año, o.nombre
                            select new
                            {
                                Año = o.año,
                                Cantidad = o.total,
                                Tipo_Unidad_Organizativa = o.nombre
                            };
                    break;
                default://Área OCDE
                    query = from o in db.Database
                    .SqlQuery<organizacionesACTI_dimension_años_Result>("organizacionesACTI_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby o.año, o.nombre
                            select new
                            {
                                Año = o.año,
                                Cantidad = o.total,
                                Área_OCDE = o.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /OrganizacionesACTI/GraficoxYear/tabla/desde/hasta
        //lista todas las OrganizacionesACTI segun el tipo de tabla, el año desde y hasta.
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from o in db.Database
                    .SqlQuery<organizacionesACTI_dimension_años_Result>("organizacionesACTI_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby o.año, o.nombre
                        select new
                        {
                            o.año,
                            o.total,
                            o.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /OrganizacionesACTI/OrganizacionesACTIUnidadOrgOCDE/yearSearch/catOrg/catOCDE
        //Lista las OrganizacionesACTI para ACTI en un año por categoria ocde y unidad organizativa
        public JsonResult OrganizacionesACTIUnidadOrgOCDE(string yearSearch, string catOrg, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var organizacionParameter = new SqlParameter("@organizacion", catOrg);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from o in db.Database
                .SqlQuery<organizacionesACTI_ocde_organizacion_Result>("organizacionesACTI_ocde_organizacion @year, @organizacion, @ocde", yearParameter, organizacionParameter, ocdeParameter)
                .ToList()
                        orderby o.nombre, o.organizacion
                        select new
                        {
                            o.total,
                            o.nombre,
                            o.organizacion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /OrganizacionesACTI/OrganizacionesACTIOCDEACTI/yearSearch/catOCDE/catACTI
        //Lista las OrganizacionesACTI para ACTI en un año por categoria ocde y nivel acti
        public JsonResult OrganizacionesACTIOCDEACTI(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from o in db.Database
                .SqlQuery<organizacionesACTI_ocde_acti_Result>("organizacionesACTI_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby o.nombre, o.acti
                        select new
                        {
                            o.total,
                            o.nombre,
                            o.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /OrganizacionesACTI/OrganizacionesACTIUnidadOrgACTI/yearSearch/catOrg/catACTI
        //Lista los OrganizacionesACTI para ACTI en un año por nivel acti y unidad organizativa
        public JsonResult OrganizacionesACTIUnidadOrgACTI(string yearSearch, string catOrg, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var organizacionParameter = new SqlParameter("@organizacion", catOrg);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from o in db.Database
                .SqlQuery<organizacionesACTI_acti_organizacion_Result>("OrganizacionesACTI_acti_organizacion @year, @organizacion, @acti", yearParameter, organizacionParameter, actiParameter)
                .ToList()
                        orderby o.nombre, o.organizacion
                        select new
                        {
                            o.total,
                            o.nombre,
                            o.organizacion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /OrganizacionesACTI/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de OrganizacionesACTI por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/OrganizacionesACTI"), "OrganizacionesACTI_área_ocde.rdlc");
                    break;
                case "2": //reporte de OrganizacionesACTI por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/OrganizacionesACTI"), "OrganizacionesACTI_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de OrganizacionesACTI por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/OrganizacionesACTI"), "OrganizacionesACTI_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de OrganizacionesACTI por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/OrganizacionesACTI"), "OrganizacionesACTI_acti_n1.rdlc");
                    break;
                case "5": //reporte de OrganizacionesACTI por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/OrganizacionesACTI"), "OrganizacionesACTI_acti_n2.rdlc");
                    break;
                case "6": //reporte de OrganizacionesACTI por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/OrganizacionesACTI"), "OrganizacionesACTI_acti_n3.rdlc");
                    break;
                case "7": //reporte de OrganizacionesACTI por naturaleza organizativa
                    path = Path.Combine(Server.MapPath("~/Reportes/OrganizacionesACTI"), "OrganizacionesACTI_naturaleza_organizativa.rdlc");
                    break;
                case "8": //reporte de OrganizacionesACTI por tipo organizativa
                    path = Path.Combine(Server.MapPath("~/Reportes/OrganizacionesACTI"), "OrganizacionesACTI_tipo_organizativa.rdlc");
                    break;
                default: //reporte de OrganizacionesACTI completo
                    path = Path.Combine(Server.MapPath("~/Reportes/OrganizacionesACTI"), "OrganizacionesACTI.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from o in db.Lista_OrganizacionesACTI
                        select new
                        {
                            Año = o.año,
                            o.Area,
                            o.Subarea,
                            o.Disciplina,
                            o.ACTI_N1,
                            o.ACTI_N2,
                            o.ACTI_N3,
                            o.NaturalezaOrg,
                            o.TipoOrg,
                            o.Cantidad
                        };
            }
            else
            {
                query = from o in db.Lista_OrganizacionesACTI.ToList()
                        where
                           o.año.ToString().CompareTo(desde) >= 0 &&
                           o.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = o.año,
                            o.Area,
                            o.Subarea,
                            o.Disciplina,
                            o.ACTI_N1,
                            o.ACTI_N2,
                            o.ACTI_N3,
                            o.NaturalezaOrg,
                            o.TipoOrg,
                            o.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}