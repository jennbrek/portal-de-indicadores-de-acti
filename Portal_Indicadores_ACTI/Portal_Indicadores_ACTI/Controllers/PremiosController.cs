﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;
namespace Portal_Indicadores_ACTI.Controllers
{
    public class PremiosController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /Premios/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Premios/getYears
        //lista los años de los registros de Premios
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_Premios
                         orderby year.Año
                         select new
                         {
                             año = year.Año
                         }).ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Premios/Listar
        //lista todos los Premios
        public JsonResult Listar()
        {
            var query = from x in db.Lista_Premios
                        select new
                        {
                            Ámbito_Geográfico = x.Ambito,
                            Tipo_Organizaciones = x.Seccion,
                            Disciplina_OCDE = x.Disciplina,
                            Año = x.Año,
                            Subárea_OCDE = x.Subarea,
                            Área_OCDE = x.Area,
                            Cantidad = x.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Premios/ListarxYear/tabla/desde/hasta
        //lista todos los Premios segun el tipo de tabla, el año (con periodo) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new System.Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2": //Subárea OCDE
                    query = from x in db.Database
                    .SqlQuery<premios_dimension_años_Result>("premios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Subárea_OCDE = x.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from x in db.Database
                    .SqlQuery<premios_dimension_años_Result>("premios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Disciplina_OCDE = x.nombre
                            };
                    break;
                case "4"://ámbito geográfico
                    query = from x in db.Database
                    .SqlQuery<premios_dimension_años_Result>("premios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Ámbito_Geográfico = x.nombre
                            };
                    break;
                case "5"://tipo organizaciones
                    query = from x in db.Database
                    .SqlQuery<premios_dimension_años_Result>("premios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Tipo_Organizaciones = x.nombre
                            };
                    break;
                default://Área OCDE
                    query = from x in db.Database
                    .SqlQuery<premios_dimension_años_Result>("premios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Área_OCDE = x.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Premios/GraficoxYear/tabla/desde/hasta
        //lista todos los Premios segun el tipo de tabla, el año (sin periodo) desde y hasta
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from x in db.Database
                    .SqlQuery<premios_dimension_años_Result>("premios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby x.año, x.nombre
                        select new
                        {
                            x.año,
                            x.total,
                            x.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Premios/PremiosAmbitoOCDE/yearSearch/catOCDE
        //Lista los Premios para ACTI en un año por categoria ocde y ámbito geográfico
        public JsonResult PremiosAmbitoOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from x in db.Database
                .SqlQuery<premios_ocde_ambGeo_Result>("premios_ocde_ambGeo @year, @ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby x.nombre, x.ambito
                        select new
                        {
                            x.total,
                            x.nombre,
                            x.ambito
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Premios/PremiosTipOrgOCDE/yearSearch/catOCDE
        //Lista los Premios para ACTI en un año por categoria ocde y tipo de organizaciones
        public JsonResult PremiosTipOrgOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from x in db.Database
                .SqlQuery<premios_ocde_seccion_Result>("premios_ocde_seccion @year, @ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby x.nombre, x.seccion
                        select new
                        {
                            x.total,
                            x.nombre,
                            x.seccion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Premios/PremiosOCDE/yearSearch/catOCDE
        //Lista Premios de un año por la clasificacion ocde 
        public JsonResult PremiosOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from x in db.Database
                .SqlQuery<premios_ocde_Result>("premios_ocde @year,@ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Premios/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de Premios por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Premios"), "Premios_área_ocde.rdlc");
                    break;
                case "2": //reporte de Premios por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Premios"), "Premios_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de Premios por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Premios"), "Premios_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de Premios por ámbito geográfico
                    path = Path.Combine(Server.MapPath("~/Reportes/Premios"), "Premios_ámbito_geográfico.rdlc");
                    break;
                case "5": //reporte de Premios por tipo organizaciones
                    path = Path.Combine(Server.MapPath("~/Reportes/Premios"), "Premios_tipo_organizaciones.rdlc");
                    break;
                default: //reporte de Premios completo
                    path = Path.Combine(Server.MapPath("~/Reportes/Premios"), "Premios.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from x in db.Lista_Premios
                        select new
                        {
                            x.Año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.Ambito,
                            x.Seccion,
                            x.Cantidad
                        };
            }
            else
            {
                query = from x in db.Lista_Premios.ToList()
                        where
                           x.Año.ToString().CompareTo(desde) >= 0 &&
                           x.Año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            x.Año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.Ambito,
                            x.Seccion,
                            x.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}