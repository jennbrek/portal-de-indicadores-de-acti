﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class ProductosProyectosController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /ProductosProyectos/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /ProductosProyectos/getYears
        //lista los años de los registros de ProductosProyectos
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_ProductosProyectos
                         orderby year.Año
                         select new
                         {
                             año = year.Año
                         }).ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProductosProyectos/Listar
        //lista todos los ProductosProyectos
        public JsonResult Listar()
        {
            var query = from x in db.Lista_ProductosProyectos
                        select new
                        {
                            Año = x.Año,
                            ACTI_N1 = x.ACTI_N1,
                            ACTI_N2 = x.ACTI_N2,
                            ACTI_N3 = x.ACTI_N3,
                            Disciplina_OCDE = x.Disciplina,
                            Subárea_OCDE = x.Subarea,
                            Área_OCDE = x.Area,
                            Clase_CIIU = x.Clase,
                            Grupo_CIIU = x.Grupo,
                            División_CIIU = x.Division,
                            Sección_CIIU = x.Seccion,
                            Tipología_COLCIENCIAS = x.COLCIENCIAS,
                            Tipología_Universidad = x.TipologiaU,
                            Cantidad = x.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProductosProyectos/ListarxYear/tabla/desde/hasta
        //lista todos los ProductosProyectos segun el tipo de tabla, el año (con periodo) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new System.Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2": //Subárea OCDE
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Subárea_OCDE = x.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Disciplina_OCDE = x.nombre
                            };
                    break;
                case "4"://acti primer nivel
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                ACTI_N1 = x.nombre
                            };
                    break;
                case "5"://acti segundo nivel
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                ACTI_N2 = x.nombre
                            };
                    break;
                case "6"://acti tercer nivel
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                ACTI_N3 = x.nombre
                            };
                    break;
                case "7"://Clase ciiu
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Clase_CIIU = x.nombre
                            };
                    break;
                case "8"://Grupo ciiu
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Grupo_CIIU = x.nombre
                            };
                    break;
                case "9"://División ciiu
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                División_CIIU = x.nombre
                            };
                    break;
                case "10"://Sección ciiu
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Sección_CIIU = x.nombre
                            };
                    break;
                case "11"://Tipología colciencias
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Tipología_COLCIENCIAS = x.nombre
                            };
                    break;
                case "12"://Tipología universidad
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Tipología_Universidad = x.nombre
                            };
                    break;
                default://Área OCDE
                    query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Área_OCDE = x.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProductosProyectos/GraficoxYear/tabla/desde/hasta
        //lista todos los ProductosProyectos segun el tipo de tabla, el año (sin periodo) desde y hasta
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from x in db.Database
                    .SqlQuery<productosProyectos_dimension_años_Result>("productosProyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby x.año, x.nombre
                        select new
                        {
                            x.año,
                            x.total,
                            x.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProductosProyectos/ProductosProyectosOCDEACTI/yearSearch/catOCDE/catACTI
        //Lista los ProductosProyectos para ACTI en un año por categoria ocde y nivel acti
        public JsonResult ProductosProyectosOCDEACTI(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from x in db.Database
                .SqlQuery<productosProyectos_ocde_acti_Result>("productosProyectos_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby x.nombre, x.acti
                        select new
                        {
                            x.total,
                            x.nombre,
                            x.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProductosProyectos/ProductosProyectosOCDECIIU/yearSearch/catOCDE/catCIIU
        //Lista los ProductosProyectos para ACTI en un año por categoria ocde y nivel ciiu
        public JsonResult ProductosProyectosOCDECIIU(string yearSearch, string catOCDE, string catCIIU)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from x in db.Database
                .SqlQuery<productosProyectos_ciiu_ocde_Result>("productosProyectos_ciiu_ocde @year, @ocde, @ciiu", yearParameter, ocdeParameter, ciiuParameter)
                .ToList()
                        orderby x.nombre, x.ocde
                        select new
                        {
                            x.total,
                            x.nombre,
                            x.ocde
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProductosProyectos/ProductosProyectosACTICIIU/yearSearch/catOCDE/catACTI
        //Lista los ProductosProyectos para ACTI en un año por nivel ciiu y acti
        public JsonResult ProductosProyectosACTICIIU(string yearSearch, string catACTI, string catCIIU)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from x in db.Database
                .SqlQuery<productosProyectos_ciiu_acti_Result>("productosProyectos_ciiu_acti @year, @acti, @ciiu", yearParameter, actiParameter, ciiuParameter)
                .ToList()
                        orderby x.nombre, x.acti
                        select new
                        {
                            x.total,
                            x.nombre,
                            x.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/ProductosProyectosCOLCIENCIASACTI/yearSearch/catACTI
        //Lista los ProductosProyectos para ACTI en un año por nivel acti y tipología colciencias
        public JsonResult ProductosProyectosCOLCIENCIASACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<productosProyectos_acti_tipologiaColciencias_Result>("productosProyectos_acti_tipologiaColciencias @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.tipologia
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.tipologia
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/ProductosProyectosTipologiaUACTI/yearSearch/catACTI
        //Lista los ProductosProyectos para ACTI en un año por nivel acti y tipología de la universidad
        public JsonResult ProductosProyectosTipologiaUACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<productosProyectos_acti_tipologiaU_Result>("productosProyectos_acti_tipologiaU @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.tipologia
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.tipologia
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProductosProyectos/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de ProductosProyectos por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_área_ocde.rdlc");
                    break;
                case "2": //reporte de ProductosProyectos por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de ProductosProyectos por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de ProductosProyectos por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_acti_n1.rdlc");
                    break;
                case "5": //reporte de ProductosProyectos por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_acti_n2.rdlc");
                    break;
                case "6": //reporte de ProductosProyectos por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_acti_n3.rdlc");
                    break;
                case "7": //reporte de ProductosProyectos por clase ciiu
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_clase_ciiu.rdlc");
                    break;
                case "8": //reporte de ProductosProyectos por grupo ciiu
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_grupo_ciiu.rdlc");
                    break;
                case "9": //reporte de ProductosProyectos por división ciiu
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_división_ciiu.rdlc");
                    break;
                case "10": //reporte de ProductosProyectos por sección ciiu
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_sección_ciiu.rdlc");
                    break;
                case "11": //reporte de ProductosProyectos por tipología colciencias
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_tipología_colciencias.rdlc");
                    break;
                case "12": //reporte de ProductosProyectos por tipología universidad
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos_tipología_u.rdlc");
                    break;
                default: //reporte de ProductosProyectos completo
                    path = Path.Combine(Server.MapPath("~/Reportes/ProductosProyectos"), "ProductosProyectos.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from x in db.Lista_ProductosProyectos
                        select new
                        {
                            x.Año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            x.COLCIENCIAS,
                            x.Clase,
                            x.Grupo,
                            x.Division,
                            x.Seccion,
                            x.TipologiaU,
                            x.Cantidad
                        };
            }
            else
            {
                query = from x in db.Lista_ProductosProyectos.ToList()
                        where
                           x.Año.ToString().CompareTo(desde) >= 0 &&
                           x.Año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            x.Año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            x.COLCIENCIAS,
                            x.Clase,
                            x.Grupo,
                            x.Division,
                            x.Seccion,
                            x.TipologiaU,
                            x.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}