﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class ProgramasEducacionSuperiorController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /ProgramasEducacionSuperior/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /ProgramasEducacionSuperior/getYears
        //lista los años de registros de programas de educación superior
        public JsonResult getYears()
        {
            var query = (from year_prog in db.Lista_ProgramasEducacionSuperior
                         orderby year_prog.Año
                         select new
                         {
                             año = year_prog.Año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProgramasEducacionSuperior/Listar
        //lista todos los programas de educación superior
        public JsonResult Listar()
        {
            var query = from p in db.Lista_ProgramasEducacionSuperior
                        select new
                        {
                            Nivel_Formación = p.Formacion,
                            Tipo_Acreditación = p.Acreditacion,
                            Disciplina_OCDE = p.Disciplina,
                            Subárea_OCDE = p.Subarea,
                            Área_OCDE = p.Area,
                            ACTI = p.ACTI,
                            Año = p.Año,
                            Periodo = p.Periodo,
                            Cantidad = p.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProgramasEducacionSuperior/ListarxYear/tabla/desde/hasta
        //lista todos los programas de educación superior segun el tipo de tabla, el año (con periodo) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from p in db.Database
                    .SqlQuery<programas_dimension_años_Result>("programas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby p.año, p.nombre
                            select new
                            {
                                Año = p.año,
                                Cantidad = p.total,
                                Subárea_OCDE = p.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from p in db.Database
                    .SqlQuery<programas_dimension_años_Result>("programas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby p.año, p.nombre
                            select new
                            {
                                Año = p.año,
                                Cantidad = p.total,
                                Disciplina_OCDE = p.nombre
                            };
                    break;
                case "4"://Nivel de formación
                    query = from p in db.Database
                    .SqlQuery<programas_dimension_años_Result>("programas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby p.año, p.nombre
                            select new
                            {
                                Año = p.año,
                                Cantidad = p.total,
                                Nivel_Formación = p.nombre
                            };
                    break;
                case "5"://Acreditación
                    query = from p in db.Database
                    .SqlQuery<programas_dimension_años_Result>("programas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby p.año,  p.nombre
                            select new
                            {
                                Año = p.año,
                                Cantidad = p.total,
                                Tipo_Acreditación = p.nombre
                            };
                    break;
                case "6"://ACTI Predominante
                    query = from p in db.Database
                    .SqlQuery<programas_dimension_años_Result>("programas_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby p.año, p.nombre
                            select new
                            {
                                Año = p.año,
                                Cantidad = p.total,
                                ACTI = p.nombre
                            };
                    break;
                default://Área ocde
                    query = from p in db.Database
                    .SqlQuery<programas_dimension_años_Result>("programas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby p.año, p.nombre
                            select new
                            {
                                Año = p.año,
                                Cantidad = p.total,
                                Área_OCDE = p.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProgramasEducacionSuperior/GraficoxYear/tabla/desde/hasta
        //lista todos los programas de educación superior segun el tipo de tabla, el año (sin periodo) desde y hasta
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from p in db.Database
                    .SqlQuery<programas_dimension_años_Result>("programas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby p.año, p.nombre
                        select new
                        {
                            p.año,
                            p.total,
                            p.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProgramasEducacionSuperior/ProgramaYearACTIOCDE/yearSearch/catOCDE
        //Lista los programas de educación superior de un año por nivel OCDE y ACTI
        public JsonResult ProgramaYearACTIOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from i in db.Database
                .SqlQuery<programas_ocde_acti_Result>("programas_ocde_acti @year, @ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby i.nombre, i.acti
                        select new
                        {
                            i.total,
                            i.nombre,
                            i.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProgramasEducacionSuperior/ProgramaYearAcreditacionOCDE/yearSearch/catOCDE
        //Lista los programas de educación superior de un año por nivel OCDE y tipo de acreditacion
        public JsonResult ProgramaYearAcreditacionOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from i in db.Database
                .SqlQuery<programas_ocde_acreditacion_Result>("programas_ocde_acreditacion @year, @ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby i.nombre, i.acreditacion
                        select new
                        {
                            i.total,
                            i.nombre,
                            i.acreditacion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProgramasEducacionSuperior/ProgramaYearFormacionOCDE/yearSearch/catOCDE
        //Lista los programas de educación superior de un año por nivel OCDE y nivel de formación
        public JsonResult ProgramaYearFormacionOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from i in db.Database
                .SqlQuery<programas_ocde_formacion_Result>("programas_ocde_formacion @year, @ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby i.nombre, i.formacion
                        select new
                        {
                            i.total,
                            i.nombre,
                            i.formacion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProgramasEducacionSuperior/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de programas de educación superior por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/ProgramasEducacionSuperior"), "ProgramasEducaciónSuperior_área_ocde.rdlc");
                    break;
                case "2": //reporte de programas de educación superior por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/ProgramasEducacionSuperior"), "ProgramasEducaciónSuperior_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de programas de educación superior por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/ProgramasEducacionSuperior"), "ProgramasEducaciónSuperior_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de programas de educación superior por acti
                    path = Path.Combine(Server.MapPath("~/Reportes/ProgramasEducacionSuperior"), "ProgramasEducaciónSuperior_acti.rdlc");
                    break;
                case "5": //reporte de programas de educación superior por nivel de formación
                    path = Path.Combine(Server.MapPath("~/Reportes/ProgramasEducacionSuperior"), "ProgramasEducaciónSuperior_formación.rdlc");
                    break;
                case "6": //reporte de programas de educación superior por acreditación
                    path = Path.Combine(Server.MapPath("~/Reportes/ProgramasEducacionSuperior"), "ProgramasEducaciónSuperior_acreditación.rdlc");
                    break;
                default: //reporte de programas de educación superior completo
                    path = Path.Combine(Server.MapPath("~/Reportes/ProgramasEducacionSuperior"), "ProgramasEducaciónSuperior.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from p in db.Lista_ProgramasEducacionSuperior
                        select new
                        {
                            Año = p.Año,
                            Periodo = p.Periodo,
                            Programas = p.Cantidad,
                            Disciplina = p.Disciplina,
                            Subarea = p.Subarea,
                            Area = p.Area,
                            Nivel = p.Formacion,
                            Acreditacion = p.Acreditacion,
                            ACTI = p.ACTI
                        };
            }
            else
            {
                query = from p in db.Lista_ProgramasEducacionSuperior.ToList()
                        where
                           p.Año.ToString().CompareTo(desde) >= 0 &&
                           p.Año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = p.Año,
                            Periodo = p.Periodo,
                            Programas = p.Cantidad,
                            Disciplina = p.Disciplina,
                            Subarea = p.Subarea,
                            Area = p.Area,
                            Nivel = p.Formacion,
                            Acreditacion = p.Acreditacion,
                            ACTI = p.ACTI
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }


    }
}