﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class ConveniosController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 88675309 // Usa este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /Convenios/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Convenios/getYears
        //lista los años de registros de convenios
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_Convenios
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Convenios/Listar
        //lista todos los convenios
        public JsonResult Listar()
        {
            var query = from c in db.Lista_Convenios
                        select new
                        {
                            Año = c.año,
                            Área_OCDE = c.Area,
                            Disciplina_OCDE = c.Disciplina,
                            Subárea_OCDE = c.Subarea,
                            ACTI_N1 = c.ACTI,
                            ACTI_N2 = c.ACTI_N2,
                            ACTI_N3 = c.ACTI_N3,
                            Clase_CIIU = c.Clase,
                            Grupo_CIIU = c.Grupo,
                            División_CIIU = c.Division,
                            Sección_CIIU = c.Seccion,
                            Ámbito_Geográfico = c.AmbGeo,
                            Naturaleza_Organización = c.NatUnidadOrg,
                            Tipo_Convenio = c.TipoConvenio,
                            Resultado_Convenio = c.Resultado,
                            Cantidad = c.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Convenios/ListarxYear/tabla/desde/hasta
        //lista todos los convenios segun el tipo de tabla, el año desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                Subárea_OCDE = c.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                Disciplina_OCDE = c.nombre
                            };
                    break;
                case "4"://acti primer nivel
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                ACTI_N1 = c.nombre
                            };
                    break;
                case "5"://acti segundo nivel
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                ACTI_N2 = c.nombre
                            };
                    break;
                case "6"://acti tercer nivel
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                ACTI_N3 = c.nombre
                            };
                    break;
                case "7"://Clase CIIU
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                Clase_CIIU = c.nombre
                            };
                    break;
                case "8"://Grupo CIIU
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                Grupo_CIIU = c.nombre
                            };
                    break;
                case "9"://División CIIU
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                División_CIIU = c.nombre
                            };
                    break;
                case "10"://Sección CIIU
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                Sección_CIIU = c.nombre
                            };
                    break;
                case "11"://Ámbito geográfico
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                Ámbito_Geográfico = c.nombre
                            };
                    break;
                case "12"://Naturaleza organización
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                Naturaleza_Organización = c.nombre
                            };
                    break;
                case "13"://Tipo convenio
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                Tipo_Convenio = c.nombre
                            };
                    break;
                case "14"://Resultado
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                Resultado_Convenio = c.nombre
                            };
                    break;
                default://Área OCDE
                    query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby c.año, c.nombre
                            select new
                            {
                                Año = c.año,
                                Cantidad = c.total,
                                Área_OCDE = c.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Convenios/GraficoxYear/tabla/desde/hasta
        //lista todos los convenios segun el tipo de tabla, el año desde y hasta.
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from c in db.Database
                    .SqlQuery<convenios_dimension_años_Result>("convenios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby c.año, c.nombre
                        select new
                        {
                            c.año,
                            c.total,
                            c.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Convenios/ConveniosCIIUOCDE/yearSearch/catCIIU/catOCDE
        //Lista los convenios para ACTI en un año por nivel ciiu y categoria ocde
        public JsonResult ConveniosCIIUOCDE(string yearSearch, string catCIIU, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from c in db.Database
                .SqlQuery<convenios_ciiu_ocde_Result>("convenios_ciiu_ocde @year, @ciiu, @ocde", yearParameter, ciiuParameter, ocdeParameter)
                .ToList()
                        orderby c.nombre, c.ocde
                        select new
                        {
                            c.total,
                            c.nombre,
                            c.ocde
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Convenios/ConveniosOCDEACTI/yearSearch/catOCDE/catACTI
        //Lista los convenios para ACTI en un año por categoria ocde y nivel acti
        public JsonResult ConveniosOCDEACTI(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from c in db.Database
                .SqlQuery<convenios_ocde_acti_Result>("convenios_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby c.nombre, c.acti
                        select new
                        {
                            c.total,
                            c.nombre,
                            c.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Convenios/ConveniosCIIUACTI/yearSearch/catCIIU/catACTI
        //Lista los convenios para ACTI en un año por nivel ciiu y nivel acti
        public JsonResult ConveniosCIIUACTI(string yearSearch, string catCIIU, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from c in db.Database
                .SqlQuery<convenios_ciiu_acti_Result>("convenios_ciiu_acti @year, @ciiu, @acti", yearParameter, ciiuParameter, actiParameter)
                .ToList()
                        orderby c.nombre, c.acti
                        select new
                        {
                            c.total,
                            c.nombre,
                            c.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Convenios/ConveniosAmbitoGeoACTI/yearSearch/catACTI
        //Lista los convenios para ACTI en un año por nivel acti y ámbito geográfico
        public JsonResult ConveniosAmbitoGeoACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from c in db.Database
                .SqlQuery<convenios_acti_ambGeo_Result>("convenios_acti_ambGeo @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby c.nombre, c.ambito
                        select new
                        {
                            c.total,
                            c.nombre,
                            c.ambito
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Convenios/ConveniosNatOrgACTI/yearSearch/catACTI
        //Lista los convenios para ACTI en un año por nivel acti y naturaleza de la organización
        public JsonResult ConveniosNatOrgACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from c in db.Database
                .SqlQuery<convenios_acti_natUnidadOrg_Result>("convenios_acti_natUnidadOrg @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby c.nombre, c.organizacion
                        select new
                        {
                            c.total,
                            c.nombre,
                            c.organizacion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Convenios/ConveniosTipoConvACTI/yearSearch/catACTI
        //Lista los convenios para ACTI en un año por nivel acti y tipo de convenio
        public JsonResult ConveniosTipoConvACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from c in db.Database
                .SqlQuery<convenios_acti_tipoConvenio_Result>("convenios_acti_tipoConvenio @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby c.nombre, c.tipo
                        select new
                        {
                            c.total,
                            c.nombre,
                            c.tipo
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Convenios/ConveniosResultadoACTI/yearSearch/catACTI
        //Lista los convenios para ACTI en un año por nivel acti y resultado del convenio
        public JsonResult ConveniosResultadoACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from c in db.Database
                .SqlQuery<convenios_acti_resultado_Result>("convenios_acti_resultado @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby c.nombre, c.resultado
                        select new
                        {
                            c.total,
                            c.nombre,
                            c.resultado
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /convenios/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de convenios por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_área_ocde.rdlc");
                    break;
                case "2": //reporte de convenios por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de convenios por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de convenios por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_acti_n1.rdlc");
                    break;
                case "5": //reporte de convenios por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_acti_n2.rdlc");
                    break;
                case "6": //reporte de convenios por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_acti_n3.rdlc");
                    break;
                case "7": //reporte de convenios por clase CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_clase_ciiu.rdlc");
                    break;
                case "8": //reporte de convenios por grupo CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_grupo_ciiu.rdlc");
                    break;
                case "9": //reporte de convenios por división CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_división_ciiu.rdlc");
                    break;
                case "10": //reporte de convenios por sección CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_sección_ciiu.rdlc");
                    break;
                case "11": //reporte de convenios por ámbito geográfico
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_ámbito_geográfico.rdlc");
                    break;
                case "12": //reporte de convenios por naturaleza de la organización
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_naturaleza_organización.rdlc");
                    break;
                case "13": //reporte de convenios por tipo de convenio
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_tipo_convenios.rdlc");
                    break;
                case "14": //reporte de convenios por resultado
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios_resultado.rdlc");
                    break;
                default: //reporte de convenios completo
                    path = Path.Combine(Server.MapPath("~/Reportes/convenios"), "Convenios.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from c in db.Lista_Convenios
                        select new
                        {
                            Año = c.año,
                            Area = c.Area,
                            Subarea = c.Subarea,
                            Disciplina = c.Disciplina,
                            ACTI = c.ACTI,
                            ACTI_N2 = c.ACTI_N2,
                            ACTI_N3 = c.ACTI_N3,
                            ClaseCIIU = c.Clase,
                            GrupoCIIU = c.Grupo,
                            DivisionesCIIU = c.Division,
                            SeccionesCIIU = c.Seccion,
                            Ambito = c.AmbGeo,
                            Naturaleza_org = c.NatUnidadOrg,
                            Resultado = c.Resultado,
                            Tipo = c.TipoConvenio,
                            Convenios = c.Cantidad
                        };
            }
            else
            {
                query = from c in db.Lista_Convenios.ToList()
                        where
                           c.año.ToString().CompareTo(desde) >= 0 &&
                           c.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = c.año,
                            Area = c.Area,
                            Subarea = c.Subarea,
                            Disciplina = c.Disciplina,
                            ACTI = c.ACTI,
                            ACTI_N2 = c.ACTI_N2,
                            ACTI_N3 = c.ACTI_N3,
                            ClaseCIIU = c.Clase,
                            GrupoCIIU = c.Grupo,
                            DivisionesCIIU = c.Division,
                            SeccionesCIIU = c.Seccion,
                            Ambito = c.AmbGeo,
                            Naturaleza_org = c.NatUnidadOrg,
                            Resultado = c.Resultado,
                            Tipo = c.TipoConvenio,
                            Convenios = c.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}