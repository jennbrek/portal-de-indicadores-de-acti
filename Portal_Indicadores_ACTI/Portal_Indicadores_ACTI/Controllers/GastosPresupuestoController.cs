﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class GastosPresupuestoController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 88675309 // Usa este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /GastosPresupuesto/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /GastosPresupuesto/getYears
        //lista los años de los registros de GastosPresupuesto
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_GastosPresupuesto
                         orderby year.año
                         select new
                         {
                             año = year.año
                         }).ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPresupuesto/Listar
        //lista todos los GastosPresupuesto
        public JsonResult Listar()
        {
            var query = from x in db.Lista_GastosPresupuesto
                        select new
                        {
                            ACTI_N1 = x.ACTI_N1,
                            ACTI_N2 = x.ACTI_N2,
                            ACTI_N3 = x.ACTI_N3,
                            Fuente_Financiación = x.fuente,
                            Categoría_Fuente_Financiación = x.CategoriaFuente,
                            Año = x.año,
                            Cantidad = x.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPresupuesto/ListarxYear/tabla/desde/hasta
        //lista todos los GastosPresupuesto segun el tipo de tabla, el año (con Mes) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://acti segundo nivel
                    query = from x in db.Database
                    .SqlQuery<gastosPresupuesto_dimension_años_Result>("gastosPresupuesto_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                ACTI_N2 = x.nombre
                            };
                    break;
                case "3"://acti tercer nivel
                    query = from x in db.Database
                    .SqlQuery<gastosPresupuesto_dimension_años_Result>("gastosPresupuesto_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                ACTI_N3 = x.nombre
                            };
                    break;
                case "4"://fuente de financiación
                    query = from x in db.Database
                    .SqlQuery<gastosPresupuesto_dimension_años_Result>("gastosPresupuesto_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Fuente_Financiación = x.nombre
                            };
                    break;
                case "5"://categoria fuente de financiación
                    query = from x in db.Database
                    .SqlQuery<gastosPresupuesto_dimension_años_Result>("gastosPresupuesto_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Categoría_Fuente_Financiación = x.nombre
                            };
                    break;
                default://acti primer nivel
                    query = from x in db.Database
                    .SqlQuery<gastosPresupuesto_dimension_años_Result>("gastosPresupuesto_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                ACTI_N1 = x.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPresupuesto/GraficoxYear/tabla/desde/hasta
        //lista todos los GastosPresupuesto segun el tipo de tabla, el año (sin Mes) desde y hasta
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from x in db.Database
                    .SqlQuery<gastosPresupuesto_dimension_años_Result>("gastosPresupuesto_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby x.año, x.nombre
                        select new
                        {
                            x.año,
                            x.total,
                            x.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPresupuesto/GastosPresupuestoYearACTI/yearSearch/catACTI
        //Lista GastosPresupuesto de un año por el nivel acti
        public JsonResult GastosPresupuestoACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from x in db.Database
                .SqlQuery<gastosPresupuesto_acti_Result>("gastosPresupuesto_acti @year,@acti", yearParameter, actiParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPresupuesto/GastosPresupuestoFinanciacion/yearSearch/fuente
        //Lista GastosPresupuesto de un año por el tipo de financiación
        public JsonResult GastosPresupuestoFinanciacion(string yearSearch, string fuente)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var fuenteParameter = new SqlParameter("@fuente", fuente);

            var query = from x in db.Database
                .SqlQuery<gastosPresupuesto_financiacion_Result>("gastosPresupuesto_financiacion @year, @fuente", yearParameter, fuenteParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPresupuesto/GastosPresupuestoFinanciacionACTI/yearSearch/catACTI/fuente
        //Lista GastosPresupuesto de un año por nivel ACTI y fuente de financiación
        public JsonResult GastosPresupuestoFinanciacionACTI(string yearSearch, string catACTI, string fuente)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var fuenteParameter = new SqlParameter("@fuente", fuente);

            var query = from x in db.Database
                .SqlQuery<gastosPresupuesto_acti_financiacion_Result>("gastosPresupuesto_acti_financiacion @year,@acti,@fuente", yearParameter, actiParameter, fuenteParameter)
                .ToList()
                        orderby x.financiacion, x.nombre
                        select new
                        {
                            x.total,
                            x.financiacion,
                            x.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPresupuesto/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de GastosPresupuesto por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPresupuesto"), "GastosPresupuesto_acti_n1.rdlc");
                    break;
                case "2": //reporte de GastosPresupuesto por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPresupuesto"), "GastosPresupuesto_acti_n2.rdlc");
                    break;
                case "3": //reporte de GastosPresupuesto por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPresupuesto"), "GastosPresupuesto_acti_n3.rdlc");
                    break;
                case "4": //reporte de GastosPresupuesto por fuente de financiación
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPresupuesto"), "GastosPresupuesto_fuente_financiación.rdlc");
                    break;
                case "5": //reporte de GastosPresupuesto por categoría fuente de financiación
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPresupuesto"), "GastosPresupuesto_categoría_fuente_financiación.rdlc");
                    break;
                default: //reporte de GastosPresupuesto completo
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPresupuesto"), "GastosPresupuesto.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from x in db.Lista_GastosPresupuesto
                        select new
                        {
                            Año=x.año,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            Fuente=x.fuente,
                            x.CategoriaFuente,
                            x.Cantidad
                        };
            }
            else
            {
                query = from x in db.Lista_GastosPresupuesto.ToList()
                        where
                           x.año.ToString().CompareTo(desde) >= 0 &&
                           x.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año=x.año,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            Fuente=x.fuente,
                            x.CategoriaFuente,
                            x.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}