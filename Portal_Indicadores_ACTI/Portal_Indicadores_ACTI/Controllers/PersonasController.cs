﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class PersonasController : Controller
    {

        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todas las Requests
            };
        }

        //
        // GET: /Personas/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Personas/getYears
        //lista las años de registros de producto
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_Personas
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Personas/Listar
        //lista todas las Personas
        public JsonResult Listar()
        {
            var query = from x in db.Lista_Personas
                        select new
                        {
                            Año = x.año,
                            Área_OCDE = x.Area,
                            Disciplina_OCDE = x.Disciplina,
                            Subárea_OCDE = x.Subarea,
                            ACTI_N1 = x.ACTI_N1,
                            ACTI_N2 = x.ACTI_N2,
                            ACTI_N3 = x.ACTI_N3,
                            Rango_Edad = x.edad,
                            Sexo = x.sexo,
                            Nivel_Formación = x.Formacion,
                            Nacionalidad = x.Nacionalidad,
                            Grupo_Étnico = x.Etnia,
                            Tipo_Vinculación = x.vinculo,
                            Años_Experiencia_ACTI = x.añosACTI,
                            Años_Experiencia_Laboral = x.añoslaboral,
                            Categoría_Investigador = x.CatColciencias,
                            Pertenencia_Redes_ACTI = x.Redes,
                            Tiempo_Dedicado_ACTI = x.TiempoDedicado,
                            CantidadTE = x.CantidadTE,
                            Cantidad = x.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Personas/ListarxYear/tabla/acti/desde/hasta
        //lista todas las Personas segun el tipo de tabla, la acti, el año desde y hasta
        public JsonResult ListarxYear(string tabla, string acti, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var actiParameter = new SqlParameter("@acti", acti);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Subárea_OCDE = x.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Disciplina_OCDE = x.nombre
                            };
                    break;
                case "4"://acti segundo nivel
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                ACTI_N2 = x.nombre
                            };
                    break;
                case "5"://acti tercer nivel
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                ACTI_N3 = x.nombre
                            };
                    break;
                case "6"://sexo
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Sexo = x.nombre
                            };
                    break;
                case "7"://edad
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Rango_Edad = x.nombre
                            };
                    break;
                case "8"://nivel de formación
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Nivel_Formación = x.nombre
                            };
                    break;
                case "9"://nacionalidad
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Nacionalidad = x.nombre
                            };
                    break;
                case "10"://tipo de vinculación
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Tipo_Vinculación = x.nombre
                            };
                    break;
                case "11"://grupo étnico
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Grupo_Étnico = x.nombre
                            };
                    break;
                case "12"://años de experiencia en acti
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Años_Experiencia_ACTI = x.nombre
                            };
                    break;
                case "13"://años de experiencia laboral
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Años_Experiencia_Laboral = x.nombre
                            };
                    break;
                case "14"://categoría de investigador
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Categoría_Investigador = x.nombre
                            };
                    break;
                case "15"://pertenencia redes acti
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Pertenencia_Redes_ACTI = x.nombre
                            };
                    break;
                case "16"://tiempo dedicado a acti
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Tiempo_Dedicado_ACTI = x.nombre
                            };
                    break;
                default://Área OCDE
                    query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Área_OCDE = x.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Personas/GraficoxYear/tabla/acti/desde/hasta
        //lista todas las Personas segun el tipo de tabla, la acti, el año desde y hasta.
        public JsonResult GraficoxYear(string tabla, string acti, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var actiParameter = new SqlParameter("@acti", acti);

            var query = from x in db.Database
                    .SqlQuery<personas_dimension_años_Result>("personas_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                        orderby x.año, x.nombre
                        select new
                        {
                            x.año,
                            x.total,
                            x.totalTE,
                            x.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Personas/PersonasLaboralOCDE/yearSearch/catOCDE/acti1
        //Lista las Personas en un año por categoría ocde y experiencia laboral
        public JsonResult PersonasLaboralOCDE(string yearSearch, string catOCDE, string acti1)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);
            var acti1Parameter = new SqlParameter("@acti1", acti1);

            var query = from x in db.Database
                .SqlQuery<personas_ocde_expLaboral_Result>("personas_ocde_expLaboral @year, @ocde, @acti1", yearParameter, ocdeParameter, acti1Parameter)
                .ToList()
                        orderby x.nombre, x.laboral
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre,
                            x.laboral
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Personas/PersonasEdadOCDE/yearSearch/catOCDE/acti1
        //Lista las Personas en un año por categoría ocde y rango edad
        public JsonResult PersonasEdadOCDE(string yearSearch, string catOCDE, string acti1)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);
            var acti1Parameter = new SqlParameter("@acti1", acti1);

            var query = from x in db.Database
                .SqlQuery<personas_ocde_edad_Result>("personas_ocde_edad @year, @ocde, @acti1", yearParameter, ocdeParameter, acti1Parameter)
                .ToList()
                        orderby x.nombre, x.edad
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre,
                            x.edad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Personas/PersonasSexoOCDE/yearSearch/catOCDE/acti1
        //Lista las Personas en un año por categoría ocde y sexo
        public JsonResult PersonasSexoOCDE(string yearSearch, string catOCDE, string acti1)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);
            var acti1Parameter = new SqlParameter("@acti1", acti1);

            var query = from x in db.Database
                .SqlQuery<personas_ocde_sexo_Result>("personas_ocde_sexo @year, @ocde, @acti1", yearParameter, ocdeParameter, acti1Parameter)
                .ToList()
                        orderby x.nombre, x.sexo
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre,
                            x.sexo
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Personas/PersonasFormacionOCDE/yearSearch/catOCDE/acti1
        //Lista las Personas en un año por categoría ocde y nivel de formación
        public JsonResult PersonasFormacionOCDE(string yearSearch, string catOCDE, string acti1)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);
            var acti1Parameter = new SqlParameter("@acti1", acti1);

            var query = from x in db.Database
                .SqlQuery<personas_ocde_formacion_Result>("personas_ocde_formacion @year, @ocde, @acti1", yearParameter, ocdeParameter, acti1Parameter)
                .ToList()
                        orderby x.nombre, x.formacion
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre,
                            x.formacion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Personas/PersonasExpACTIOCDE/yearSearch/catOCDE/acti1
        //Lista las Personas en un año por categoría ocde y experiencia acti
        public JsonResult PersonasExpACTIOCDE(string yearSearch, string catOCDE, string acti1)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);
            var acti1Parameter = new SqlParameter("@acti1", acti1);

            var query = from x in db.Database
                .SqlQuery<personas_ocde_expActi_Result>("personas_ocde_expActi @year, @ocde, @acti1", yearParameter, ocdeParameter, acti1Parameter)
                .ToList()
                        orderby x.nombre, x.añosACTI
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre,
                            x.añosACTI
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Personas/PersonasInvestigadorOCDE/yearSearch/catOCDE/acti1
        //Lista las Personas en un año por categoría ocde y categoría de investigador
        public JsonResult PersonasInvestigadorOCDE(string yearSearch, string catOCDE, string acti1)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);
            var acti1Parameter = new SqlParameter("@acti1", acti1);

            var query = from x in db.Database
                .SqlQuery<personas_ocde_catInvestigador_Result>("personas_ocde_catInvestigador @year, @ocde, @acti1", yearParameter, ocdeParameter, acti1Parameter)
                .ToList()
                        orderby x.nombre, x.colciencias
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre,
                            x.colciencias
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }


        // GET: /Personas/Reporte/id/desde/hasta/tipoReporte/fileName/acti
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string acti, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de Personas por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_área_ocde.rdlc");
                    break;
                case "2": //reporte de Personas por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de Personas por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de Personas por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_acti_n2.rdlc");
                    break;
                case "5": //reporte de Personas por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_acti_n3.rdlc");
                    break;
                case "6": //reporte de Personas por sexo
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_sexo.rdlc");
                    break;
                case "7": //reporte de Personas por edad
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_rango_edad.rdlc");
                    break;
                case "8": //reporte de Personas por nivel de formación
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_nivel_formación.rdlc");
                    break;
                case "9": //reporte de Personas por nacionalidad
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_nacionalidad.rdlc");
                    break;
                case "10": //reporte de Personas por tipo de vinculación
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_tipo_vinculación.rdlc");
                    break;
                case "11": //reporte de Personas por naturaleza del producto
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_grupo_étnico.rdlc");
                    break;
                case "12": //reporte de Personas por años de experiencia acti
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_años_experiencia_acti.rdlc");
                    break;
                case "13": //reporte de Personas por años de experiencia laboral
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_años_experiencia_laboral.rdlc");
                    break;
                case "14": //reporte de Personas por categoría investigador
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_categoría_investigador.rdlc");
                    break;
                case "15": //reporte de Personas por pertenencia a redes acti
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_pertenencia_redes_acti.rdlc");
                    break;
                case "16": //reporte de Personas por tiempo dedicado a acti
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas_tiempo_dedicado_acti.rdlc");
                    break;
                default: //reporte de Personas completo
                    path = Path.Combine(Server.MapPath("~/Reportes/Personas"), "Personas.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todas las años
            {
                query = from x in db.Lista_Personas.ToList()
                        where x.ACTI_N1.ToString().Equals(acti)
                        select new
                        {
                            Año = x.año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            x.añosACTI,
                            x.añoslaboral,
                            x.CatColciencias,
                            x.Etnia,
                            Edad = x.edad,
                            Sexo = x.sexo,
                            x.Nacionalidad,
                            x.Redes,
                            Vinculo = x.vinculo,
                            x.Formacion,
                            x.Periodo,
                            x.TiempoDedicado,
                            x.CantidadTE,
                            x.Cantidad
                        };
            }
            else
            {
                query = from x in db.Lista_Personas.ToList()
                        where
                           x.año.ToString().CompareTo(desde) >= 0 &&
                           x.año.ToString().CompareTo(hasta) <= 0 &&
                           x.ACTI_N1.ToString().Equals(acti)
                        select new
                        {
                            Año = x.año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            x.añosACTI,
                            x.añoslaboral,
                            x.CatColciencias,
                            x.Etnia,
                            Edad = x.edad,
                            Sexo = x.sexo,
                            x.Nacionalidad,
                            x.Redes,
                            Vinculo = x.vinculo,
                            x.Formacion,
                            x.Periodo,
                            x.TiempoDedicado,
                            x.CantidadTE,
                            x.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}