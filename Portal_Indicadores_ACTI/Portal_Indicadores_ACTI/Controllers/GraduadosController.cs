﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class GraduadosController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 88675309 // Usa este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /Graduados/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Graduados/getYears
        //lista los años en que hubo graduados
        public JsonResult getYears()
        {
            var query = (from year_grad in db.Lista_Graduados
                         orderby year_grad.año
                         select new
                         {
                             año = year_grad.año
                         }).ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Graduados/Listar
        //lista todos los graduados
        public JsonResult Listar()
        {
            var query = from g in db.Lista_Graduados
                        select new
                        {
                            Nivel_Formación = g.Formacion,
                            Fuente_Financiación = g.Financiacion,
                            Disciplina_OCDE = g.Disciplina,
                            Año = g.año,
                            Periodo = g.Periodo,
                            Subárea_OCDE = g.Subarea,
                            Área_OCDE = g.Area,
                            Cantidad = g.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Graduados/ListarxYear/tabla/desde/hasta
        //lista todos los graduados segun el tipo de tabla, el año (con periodo) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2": //Subárea OCDE
                    query = from g in db.Database
                    .SqlQuery<graduados_dimension_Result>("graduados_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby g.año, g.semestre, g.nombre
                            select new
                            {
                                Año = g.año,
                                Periodo = g.semestre,
                                Cantidad = g.total,
                                Subárea_OCDE = g.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from g in db.Database
                    .SqlQuery<graduados_dimension_Result>("graduados_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby g.año, g.semestre, g.nombre
                            select new
                            {
                                Año = g.año,
                                Periodo = g.semestre,
                                Cantidad = g.total,
                                Disciplina_OCDE = g.nombre
                            };
                    break;
                case "4"://Nivel de formación
                    query = from g in db.Database
                    .SqlQuery<graduados_dimension_Result>("graduados_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby g.año, g.semestre, g.nombre
                            select new
                            {
                                Año = g.año,
                                Periodo = g.semestre,
                                Cantidad = g.total,
                                Nivel_Formación = g.nombre
                            };
                    break;
                case "5"://Fuente de financiación
                    query = from g in db.Database
                    .SqlQuery<graduados_dimension_Result>("graduados_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby g.año, g.semestre, g.nombre
                            select new
                            {
                                Año = g.año,
                                Periodo = g.semestre,
                                Cantidad = g.total,
                                Fuente_Financiación = g.nombre
                            };
                    break;
                default://Área OCDE
                    query = from g in db.Database
                    .SqlQuery<graduados_dimension_Result>("graduados_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby g.año, g.semestre, g.nombre
                            select new
                            {
                                Año = g.año,
                                Periodo = g.semestre,
                                Cantidad = g.total,
                                Área_OCDE = g.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Graduados/GraficoxYear/tabla/desde/hasta
        //lista todos los graduados segun el tipo de tabla, el año (sin periodo) desde y hasta
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from g in db.Database
                    .SqlQuery<graduados_dimension_años_Result>("graduados_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby g.año, g.nombre
                        select new
                        {
                            g.año,
                            g.total,
                            g.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Graduados/GradsCatOCDE/yearSearch
        //Lista graduados en un año por tipo ocde
        public JsonResult GradsCatOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from g in db.Database
                .SqlQuery<graduados_ocde_Result>("graduados_ocde @year, @ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby g.nombre
                        select new
                        {
                            g.total,
                            g.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Graduados/GradsYearFormacionOCDE/yearSearch/catOCDE
        //Lista graduados de un año por nivel OCDE y nivel de formacion
        public JsonResult GradsYearFormacionOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from g in db.Database
                .SqlQuery<graduados_ocde_formacion_Result>("graduados_ocde_formacion @year,@ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby g.formacion, g.nombre
                        select new
                        {
                            g.total,
                            g.formacion,
                            g.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Graduados/GradsYearFinanciacionOCDE/yearSearch/catOCDE
        //Lista graduados de un año por nivel OCDE y fuente de financiación
        public JsonResult GradsYearFinanciacionOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from g in db.Database
                .SqlQuery<graduados_ocde_financiacion_Result>("graduados_ocde_financiacion @year,@ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby g.financiacion, g.nombre
                        select new
                        {
                            g.total,
                            g.financiacion,
                            g.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Graduados/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de graduados por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Graduados"), "Graduados_área_ocde.rdlc");
                    break;
                case "2": //reporte de graduados por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Graduados"), "Graduados_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de graduados por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Graduados"), "Graduados_disciplina_ocde.rdlc");
                    break;
                case "5": //reporte de graduados por fuente de financiación
                    path = Path.Combine(Server.MapPath("~/Reportes/Graduados"), "Graduados_financiación.rdlc");
                    break;
                case "4": //reporte de graduados por nivel de formación
                    path = Path.Combine(Server.MapPath("~/Reportes/Graduados"), "Graduados_formación.rdlc");
                    break;
                default: //reporte de graduados completo
                    path = Path.Combine(Server.MapPath("~/Reportes/Graduados"), "Graduados.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }
            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from g in db.Lista_Graduados
                        select new
                        {
                            Año = g.año,
                            Periodo = g.Periodo,
                            Graduados = g.Cantidad,
                            Disciplina = g.Disciplina,
                            Subarea = g.Subarea,
                            Area = g.Area,
                            Fuente = g.Financiacion,
                            Nivel = g.Formacion
                        };
            }
            else
            {
                query = from g in db.Lista_Graduados.ToList()
                        where
                           g.año.ToString().CompareTo(desde) >= 0 &&
                           g.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = g.año,
                            Periodo = g.Periodo,
                            Graduados = g.Cantidad,
                            Disciplina = g.Disciplina,
                            Subarea = g.Subarea,
                            Area = g.Area,
                            Fuente = g.Financiacion,
                            Nivel = g.Formacion
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}