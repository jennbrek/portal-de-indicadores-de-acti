﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class RecursosBibliograficosController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /RecursosBibliograficos/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /RecursosBibliograficos/getYears
        //lista los años de los registros de recursos bibliográficos
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_RecursosBibliograficos
                         orderby year.año
                         select new
                         {
                             año = year.año
                         }).ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /RecursosBibliograficos/Listar
        //lista todos los recursos bibliográficos
        public JsonResult Listar()
        {
            var query = from b in db.Lista_RecursosBibliograficos
                        select new
                        {
                            Disciplina_OCDE = b.Disciplina,
                            Año = b.año,
                            Periodo = b.Periodo,
                            Subárea_OCDE = b.Subarea,
                            Área_OCDE = b.Area,
                            Cantidad = b.Cantidad,
                            Naturaleza_Recurso = b.tipoRecurso
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /RecursosBibliograficos/ListarxYear/tabla/desde/hasta
        //lista todos los recursos bibliográficos segun el tipo de tabla, el año (con periodo) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2": //Subárea OCDE
                    query = from b in db.Database
                    .SqlQuery<recursosBibliograficos_dimension_años_Result>("recursosBibliograficos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby b.año, b.nombre
                            select new
                            {
                                Año = b.año,
                                Cantidad = b.total,
                                Subárea_OCDE = b.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from b in db.Database
                    .SqlQuery<recursosBibliograficos_dimension_años_Result>("recursosBibliograficos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby b.año, b.nombre
                            select new
                            {
                                Año = b.año,
                                Cantidad = b.total,
                                Disciplina_OCDE = b.nombre
                            };
                    break;
                case "4"://Naturaleza del recurso
                    query = from b in db.Database
                    .SqlQuery<recursosBibliograficos_dimension_años_Result>("recursosBibliograficos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby b.año, b.nombre
                            select new
                            {
                                Año = b.año,
                                Cantidad = b.total,
                                Naturaleza_Recurso = b.nombre
                            };
                    break;
                default://Área OCDE
                    query = from b in db.Database
                    .SqlQuery<recursosBibliograficos_dimension_años_Result>("recursosBibliograficos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby b.año, b.nombre
                            select new
                            {
                                Año = b.año,
                                Cantidad = b.total,
                                Área_OCDE = b.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /RecursosBibliograficos/GraficoxYear/tabla/desde/hasta
        //lista todos los recursos bibliográficos segun el tipo de tabla, el año (sin periodo) desde y hasta
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from b in db.Database
                    .SqlQuery<recursosBibliograficos_dimension_años_Result>("recursosBibliograficos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby b.año, b.nombre
                        select new
                        {
                            b.año,
                            b.total,
                            b.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /RecursosBibliograficos/BiblioYearRecursoOCDE/yearSearch/catOCDE
        //Lista recursos bibliográficos de un año por nivel OCDE y naturaleza del recurso
        public JsonResult BiblioYearRecursoOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from b in db.Database
                .SqlQuery<recursosBibliograficos_ocde_tipoRecurso_Result>("recursosBibliograficos_ocde_tipoRecurso @year,@ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby b.tipo, b.nombre
                        select new
                        {
                            b.total,
                            b.tipo,
                            b.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /RecursosBibliograficos/BiblioYearRecurso/yearSearch
        //Lista recursos bibliográficos de un año por la naturaleza del recurso
        public JsonResult BiblioYearRecurso(string yearSearch)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);

            var query = from b in db.Database
                .SqlQuery<recursosBibliograficos_tipoRecurso_Result>("recursosBibliograficos_tipoRecurso @year", yearParameter)
                .ToList()
                        orderby b.nombre
                        select new
                        {
                            b.total,
                            b.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /RecursosBibliograficos/BiblioYearOCDE/yearSearch/catOCDE
        //Lista recursos bibliográficos de un año por la clasificacion ocde
        public JsonResult BiblioYearOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from b in db.Database
                .SqlQuery<recursosBibliograficos_ocde_Result>("recursosBibliograficos_ocde @year,@ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby b.nombre
                        select new
                        {
                            b.total,
                            b.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /RecursosBibliograficos/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de recursos bibliográficos por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/RecursosBibliograficos"), "RecursosBibliográficos_área_ocde.rdlc");
                    break;
                case "2": //reporte de recursos bibliográficos por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/RecursosBibliograficos"), "RecursosBibliográficos_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de recursos bibliográficos por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/RecursosBibliograficos"), "RecursosBibliográficos_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de recursos bibliográficos por naturaleza del recurso
                    path = Path.Combine(Server.MapPath("~/Reportes/RecursosBibliograficos"), "RecursosBibliográficos_naturaleza_recurso.rdlc");
                    break;
                default: //reporte de recursos bibliográficos completo
                    path = Path.Combine(Server.MapPath("~/Reportes/RecursosBibliograficos"), "RecursosBibliográficos.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }
            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from b in db.Lista_RecursosBibliograficos
                        select new
                        {
                            Disciplina = b.Disciplina,
                            Año = b.año,
                            Periodo = b.Periodo,
                            Subarea = b.Subarea,
                            Area = b.Area,
                            Recursos = b.Cantidad,
                            Tipo = b.tipoRecurso
                        };
            }
            else
            {
                query = from b in db.Lista_RecursosBibliograficos.ToList()
                        where
                           b.año.ToString().CompareTo(desde) >= 0 &&
                           b.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Disciplina = b.Disciplina,
                            Año = b.año,
                            Periodo = b.Periodo,
                            Subarea = b.Subarea,
                            Area = b.Area,
                            Recursos = b.Cantidad,
                            Tipo = b.tipoRecurso
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}