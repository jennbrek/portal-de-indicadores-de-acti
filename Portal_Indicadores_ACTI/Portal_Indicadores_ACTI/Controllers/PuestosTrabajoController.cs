﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class PuestosTrabajoController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /PuestosTrabajo/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /PuestosTrabajo/getYears
        //lista los años de los registros de PuestosTrabajo
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_PuestosTrabajos
                         orderby year.año
                         select new
                         {
                             año = year.año
                         }).ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PuestosTrabajo/Listar
        //lista todos los PuestosTrabajo
        public JsonResult Listar()
        {
            var query = from x in db.Lista_PuestosTrabajos
                        select new
                        {
                            ACTI_N1 = x.ACTI_N1,
                            ACTI_N2 = x.ACTI_N2,
                            ACTI_N3 = x.ACTI_N3,
                            Disciplina_OCDE = x.Disciplina,
                            Año = x.año,
                            Periodo = x.Periodo,
                            Subárea_OCDE = x.Subarea,
                            Área_OCDE = x.Area,
                            Cantidad = x.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PuestosTrabajo/ListarxYear/tabla/desde/hasta
        //lista todos los PuestosTrabajo segun el tipo de tabla, el año (con periodo) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new System.Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2": //Subárea OCDE
                    query = from x in db.Database
                    .SqlQuery<puestosTrabajos_dimension_Result>("puestosTrabajos_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.semestre, x.nombre
                            select new
                            {
                                Año = x.año,
                                Periodo = x.semestre,
                                Cantidad = x.total,
                                Subárea_OCDE = x.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from x in db.Database
                    .SqlQuery<puestosTrabajos_dimension_Result>("puestosTrabajos_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.semestre, x.nombre
                            select new
                            {
                                Año = x.año,
                                Periodo = x.semestre,
                                Cantidad = x.total,
                                Disciplina_OCDE = x.nombre
                            };
                    break;
                case "4"://acti primer nivel
                    query = from x in db.Database
                    .SqlQuery<puestosTrabajos_dimension_Result>("puestosTrabajos_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.semestre, x.nombre
                            select new
                            {
                                Año = x.año,
                                Periodo = x.semestre,
                                Cantidad = x.total,
                                ACTI_N1 = x.nombre
                            };
                    break;
                case "5"://acti segundo nivel
                    query = from x in db.Database
                    .SqlQuery<puestosTrabajos_dimension_Result>("puestosTrabajos_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.semestre, x.nombre
                            select new
                            {
                                Año = x.año,
                                Periodo = x.semestre,
                                Cantidad = x.total,
                                ACTI_N2 = x.nombre
                            };
                    break;
                case "6"://acti tercer nivel
                    query = from x in db.Database
                    .SqlQuery<puestosTrabajos_dimension_Result>("puestosTrabajos_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.semestre, x.nombre
                            select new
                            {
                                Año = x.año,
                                Periodo = x.semestre,
                                Cantidad = x.total,
                                ACTI_N3 = x.nombre
                            };
                    break;
                default://Área OCDE
                    query = from x in db.Database
                    .SqlQuery<puestosTrabajos_dimension_Result>("puestosTrabajos_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.semestre, x.nombre
                            select new
                            {
                                Año = x.año,
                                Periodo = x.semestre,
                                Cantidad = x.total,
                                Área_OCDE = x.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PuestosTrabajo/GraficoxYear/tabla/desde/hasta
        //lista todos los PuestosTrabajo segun el tipo de tabla, el año (sin periodo) desde y hasta
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from x in db.Database
                    .SqlQuery<puestosTrabajos_dimension_años_Result>("puestosTrabajos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby x.año, x.nombre
                        select new
                        {
                            x.año,
                            x.total,
                            x.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PuestosTrabajo/PuestosTrabajoOCDEACTI/yearSearch/catOCDE/catACTI
        //Lista los PuestosTrabajo para ACTI en un año por categoria ocde y nivel acti
        public JsonResult PuestosTrabajoOCDEACTI(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from x in db.Database
                .SqlQuery<puestosTrabajos_ocde_acti_Result>("puestosTrabajos_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby x.nombre, x.acti
                        select new
                        {
                            x.total,
                            x.nombre,
                            x.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PuestosTrabajo/PuestosTrabajoYearACTI/yearSearch/catACTI
        //Lista PuestosTrabajo de un año por el nivel acti
        public JsonResult PuestosTrabajoYearACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from x in db.Database
                .SqlQuery<puestosTrabajos_acti_Result>("puestosTrabajos_acti @year,@acti", yearParameter, actiParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PuestosTrabajo/PuestosTrabajoYearOCDE/yearSearch/catOCDE
        //Lista PuestosTrabajo de un año por la clasificacion ocde
        public JsonResult PuestosTrabajoYearOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from x in db.Database
                .SqlQuery<puestosTrabajos_ocde_Result>("puestosTrabajos_ocde @year,@ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PuestosTrabajo/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de PuestosTrabajo por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/PuestosTrabajo"), "PuestosTrabajo_área_ocde.rdlc");
                    break;
                case "2": //reporte de PuestosTrabajo por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/PuestosTrabajo"), "PuestosTrabajo_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de PuestosTrabajo por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/PuestosTrabajo"), "PuestosTrabajo_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de PuestosTrabajo por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/PuestosTrabajo"), "PuestosTrabajo_acti_n1.rdlc");
                    break;
                case "5": //reporte de PuestosTrabajo por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/PuestosTrabajo"), "PuestosTrabajo_acti_n2.rdlc");
                    break;
                case "6": //reporte de PuestosTrabajo por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/PuestosTrabajo"), "PuestosTrabajo_acti_n3.rdlc");
                    break;
                default: //reporte de PuestosTrabajo completo
                    path = Path.Combine(Server.MapPath("~/Reportes/PuestosTrabajo"), "PuestosTrabajo.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from x in db.Lista_PuestosTrabajos
                        select new
                        {
                            Año = x.año,
                            x.Periodo,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            x.Cantidad
                        };
            }
            else
            {
                query = from x in db.Lista_PuestosTrabajos.ToList()
                        where
                           x.año.ToString().CompareTo(desde) >= 0 &&
                           x.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = x.año,
                            x.Periodo,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            x.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}