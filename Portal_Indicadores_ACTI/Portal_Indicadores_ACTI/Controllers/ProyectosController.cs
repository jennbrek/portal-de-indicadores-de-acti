﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class ProyectosController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 88675309 // Usa este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /Proyectos/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Proyectos/getYears
        //lista los años de registros de Proyectos
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_Proyectos
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/Listar
        //lista todas los Proyectos
        public JsonResult Listar()
        {
            var query = from r in db.Lista_Proyectos
                        select new
                        {
                            Año = r.año,
                            Área_OCDE = r.Area,
                            Disciplina_OCDE = r.Disciplina,
                            Subárea_OCDE = r.Subarea,
                            ACTI_N1 = r.ACTI_N1,
                            ACTI_N2 = r.ACTI_N2,
                            ACTI_N3 = r.ACTI_N3,
                            Clase_CIIU = r.Clase,
                            Grupo_CIIU = r.Grupo,
                            División_CIIU = r.Division,
                            Sección_CIIU = r.Seccion,
                            Ámbito_Geográfico = r.Ambito,
                            Estado_Proyecto = r.Estado,
                            Tipología_COLCIENCIAS = r.Colciencias,
                            Tipología_Universidad = r.TipologiaU,
                            Categoría_Fuente_Financiación = r.CategoriaFuente,
                            Fuente_Financiación = r.Fuente,
                            Cantidad = r.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/ListarxYear/tabla/desde/hasta
        //lista todas los Proyectos segun el tipo de tabla, el año desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Subárea_OCDE = r.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Disciplina_OCDE = r.nombre
                            };
                    break;
                case "4"://acti primer nivel
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N1 = r.nombre
                            };
                    break;
                case "5"://acti segundo nivel
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N2 = r.nombre
                            };
                    break;
                case "6"://acti tercer nivel
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N3 = r.nombre
                            };
                    break;
                case "7"://Clase CIIU
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Clase_CIIU = r.nombre
                            };
                    break;
                case "8"://Grupo CIIU
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Grupo_CIIU = r.nombre
                            };
                    break;
                case "9"://División CIIU
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                División_CIIU = r.nombre
                            };
                    break;
                case "10"://Sección CIIU
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Sección_CIIU = r.nombre
                            };
                    break;
                case "11"://Ámbito geográfico
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Ámbito_Geográfico = r.nombre
                            };
                    break;
                case "12"://Estado del proyecto
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Estado_Proyecto = r.nombre
                            };
                    break;
                case "13"://Tipología colciencias
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Tipología_COLCIENCIAS = r.nombre
                            };
                    break;
                case "14"://Tipología Universidad
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Tipología_Universidad = r.nombre
                            };
                    break;
                case "15"://Fuente de financiación
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Fuente_Financiación = r.nombre
                            };
                    break;
                case "16"://categoria fuente de financiación
                    query = from x in db.Database
                    .SqlQuery<gastos_dimension_Result>("gastos_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.mes, x.nombre
                            select new
                            {
                                Año = x.año,
                                Mes = x.mes,
                                Cantidad = x.total,
                                Categoría_Fuente_Financiación = x.nombre
                            };
                    break;
                default://Área OCDE
                    query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Área_OCDE = r.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/GraficoxYear/tabla/desde/hasta
        //lista todas los Proyectos segun el tipo de tabla, el año desde y hasta.
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from r in db.Database
                    .SqlQuery<proyectos_dimension_años_Result>("proyectos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby r.año, r.nombre
                        select new
                        {
                            r.año,
                            r.total,
                            r.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/ProyectosCIIUOCDE/yearSearch/catCIIU/catOCDE
        //Lista los Proyectos para ACTI en un año por nivel ciiu y categoria ocde
        public JsonResult ProyectosCIIUOCDE(string yearSearch, string catCIIU, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from r in db.Database
                .SqlQuery<proyectos_ciiu_ocde_Result>("proyectos_ciiu_ocde @year, @ciiu, @ocde", yearParameter, ciiuParameter, ocdeParameter)
                .ToList()
                        orderby r.nombre, r.ocde
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.ocde
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/ProyectosOCDEACTI/yearSearch/catOCDE/catACTI
        //Lista los Proyectos para ACTI en un año por categoria ocde y nivel acti
        public JsonResult ProyectosOCDEACTI(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from r in db.Database
                .SqlQuery<proyectos_ocde_acti_Result>("proyectos_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.acti
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/ProyectosCIIUACTI/yearSearch/catCIIU/catACTI
        //Lista los Proyectos para ACTI en un año por nivel ciiu y nivel acti
        public JsonResult ProyectosCIIUACTI(string yearSearch, string catCIIU, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<proyectos_ciiu_acti_Result>("proyectos_ciiu_acti @year, @ciiu, @acti", yearParameter, ciiuParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.acti
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/ProyectosAmbitoGeoACTI/yearSearch/catACTI
        //Lista los Proyectos para ACTI en un año por nivel acti y ámbito geográfico
        public JsonResult ProyectosAmbitoGeoACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<proyectos_acti_ambGeo_Result>("proyectos_acti_ambGeo @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.ambito
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.ambito
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/ProyectosEstadoACTI/yearSearch/catACTI
        //Lista los Proyectos para ACTI en un año por nivel acti y estado del proyecto
        public JsonResult ProyectosEstadoACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<proyectos_acti_estado_Result>("proyectos_acti_estado @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.estado
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.estado
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/ProyectosCOLCIENCIASACTI/yearSearch/catACTI
        //Lista los Proyectos para ACTI en un año por nivel acti y tipología colciencias
        public JsonResult ProyectosCOLCIENCIASACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<proyectos_acti_tipologiaColciencias_Result>("proyectos_acti_tipologiaColciencias @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.tipologia
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.tipologia
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/ProyectosTipologiaUACTI/yearSearch/catACTI
        //Lista los Proyectos para ACTI en un año por nivel acti y tipología de la universidad
        public JsonResult ProyectosTipologiaUACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<proyectos_acti_tipologiaU_Result>("proyectos_acti_tipologiaU @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.tipologia
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.tipologia
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/ProyectosFinanciacionACTI/yearSearch/catACTI/fuente
        //Lista los Proyectos para ACTI en un año por nivel acti y fuente de financiación
        public JsonResult ProyectosFinanciacionACTI(string yearSearch, string catACTI, string fuente)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var fuenteParameter = new SqlParameter("@fuente", fuente);

            var query = from r in db.Database
                .SqlQuery<proyectos_acti_financiacion_Result>("proyectos_acti_financiacion @year, @acti, @fuente", yearParameter, actiParameter, fuenteParameter)
                .ToList()
                        orderby r.nombre, r.financiacion
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.financiacion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Proyectos/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de Proyectos por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_área_ocde.rdlc");
                    break;
                case "2": //reporte de Proyectos por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de Proyectos por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de Proyectos por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_acti_n1.rdlc");
                    break;
                case "5": //reporte de Proyectos por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_acti_n2.rdlc");
                    break;
                case "6": //reporte de Proyectos por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_acti_n3.rdlc");
                    break;
                case "7": //reporte de Proyectos por clase CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_clase_ciiu.rdlc");
                    break;
                case "8": //reporte de Proyectos por grupo CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_grupo_ciiu.rdlc");
                    break;
                case "9": //reporte de Proyectos por división CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_división_ciiu.rdlc");
                    break;
                case "10": //reporte de Proyectos por sección CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_sección_ciiu.rdlc");
                    break;
                case "11": //reporte de Proyectos por ámbito geográfico
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_ámbito_geográfico.rdlc");
                    break;
                case "12": //reporte de Proyectos por estado del proyecto
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_estado.rdlc");
                    break;
                case "13": //reporte de Proyectos por tipología colciencias
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_tipología_colciencias.rdlc");
                    break;
                case "14": //reporte de Proyectos por tipología de la universidad
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_tipología_u.rdlc");
                    break;
                case "15": //reporte de Proyectos por fuente de financiación
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_fuente_financiación.rdlc");
                    break;
                case "16": //reporte de Proyectos por categoría fuente de financiación
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "proyectos_categoría_fuente_financiación.rdlc");
                    break;
                default: //reporte de Proyectos completo
                    path = Path.Combine(Server.MapPath("~/Reportes/Proyectos"), "Proyectos.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from r in db.Lista_Proyectos
                        select new
                        {
                            Año = r.año,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Clase,
                            r.Grupo,
                            r.Division,
                            r.Seccion,
                            r.Ambito,
                            r.Estado,
                            r.Colciencias,
                            r.TipologiaU,
                            r.Fuente,
                            r.CategoriaFuente,
                            r.Cantidad
                        };
            }
            else
            {
                query = from r in db.Lista_Proyectos.ToList()
                        where
                           r.año.ToString().CompareTo(desde) >= 0 &&
                           r.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = r.año,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Clase,
                            r.Grupo,
                            r.Division,
                            r.Seccion,
                            r.Ambito,
                            r.Estado,
                            r.Colciencias,
                            r.TipologiaU,
                            r.Fuente,
                            r.CategoriaFuente,
                            r.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}