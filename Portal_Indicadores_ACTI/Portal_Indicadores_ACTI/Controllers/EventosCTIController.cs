﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class EventosCTIController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 88675309 // Usa este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /EventosCTI/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /EventosCTI/getYears
        //lista los años de registros de EventosCTI
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_EventosCTI
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EventosCTI/Listar
        //lista todos los EventosCTI
        public JsonResult Listar()
        {
            var query = from r in db.Lista_EventosCTI
                        select new
                        {
                            Año = r.año,
                            Periodo = r.Periodo,
                            Área_OCDE = r.Area,
                            Disciplina_OCDE = r.Disciplina,
                            Subárea_OCDE = r.Subarea,
                            ACTI_N1 = r.ACTI_N1,
                            ACTI_N2 = r.ACTI_N2,
                            ACTI_N3 = r.ACTI_N3,
                            Clase_CIIU = r.Clase,
                            Grupo_CIIU = r.Grupo,
                            División_CIIU = r.Division,
                            Sección_CIIU = r.Seccion,
                            Naturaleza_Evento = r.natEvento,
                            Cantidad = r.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EventosCTI/ListarxYear/tabla/desde/hasta
        //lista todos los EventosCTI segun el tipo de tabla, el año (con semestre) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_Result>("eventosCTI_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Periodo = r.semestre,
                                Cantidad = r.total,
                                Subárea_OCDE = r.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_Result>("eventosCTI_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Periodo = r.semestre,
                                Cantidad = r.total,
                                Disciplina_OCDE = r.nombre
                            };
                    break;
                case "4"://acti primer nivel
                    query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_Result>("eventosCTI_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Periodo = r.semestre,
                                Cantidad = r.total,
                                ACTI_N1 = r.nombre
                            };
                    break;
                case "5"://acti segundo nivel
                    query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_Result>("eventosCTI_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Periodo = r.semestre,
                                Cantidad = r.total,
                                ACTI_N2 = r.nombre
                            };
                    break;
                case "6"://acti tercer nivel
                    query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_Result>("eventosCTI_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Periodo = r.semestre,
                                Cantidad = r.total,
                                ACTI_N3 = r.nombre
                            };
                    break;
                case "7"://Clase CIIU
                    query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_Result>("eventosCTI_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Periodo = r.semestre,
                                Cantidad = r.total,
                                Clase_CIIU = r.nombre
                            };
                    break;
                case "8"://Grupo CIIU
                    query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_Result>("eventosCTI_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Periodo = r.semestre,
                                Cantidad = r.total,
                                Grupo_CIIU = r.nombre
                            };
                    break;
                case "9"://División CIIU
                    query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_Result>("eventosCTI_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Periodo = r.semestre,
                                Cantidad = r.total,
                                División_CIIU = r.nombre
                            };
                    break;
                case "10"://Sección CIIU
                    query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_Result>("eventosCTI_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Periodo = r.semestre,
                                Cantidad = r.total,
                                Sección_CIIU = r.nombre
                            };
                    break;
                case "11"://Naturaleza del evento
                    query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_Result>("eventosCTI_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Periodo = r.semestre,
                                Cantidad = r.total,
                                Naturaleza_Evento = r.nombre
                            };
                    break;
                default://Área OCDE
                    query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_Result>("eventosCTI_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Periodo = r.semestre,
                                Cantidad = r.total,
                                Área_OCDE = r.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EventosCTI/GraficoxYear/tabla/desde/hasta
        //lista todos los EventosCTI segun el tipo de tabla, el año (sin semestre) desde y hasta.
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from r in db.Database
                    .SqlQuery<eventosCTI_dimension_años_Result>("eventosCTI_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby r.año, r.nombre
                        select new
                        {
                            r.año,
                            r.total,
                            r.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EventosCTI/EventosCTICIIUOCDE/yearSearch/catCIIU/catOCDE
        //Lista los EventosCTI en un año por nivel ciiu y categoria ocde
        public JsonResult EventosCTICIIUOCDE(string yearSearch, string catCIIU, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from r in db.Database
                .SqlQuery<eventosCTI_ciiu_ocde_Result>("eventosCTI_ciiu_ocde @year, @ciiu, @ocde", yearParameter, ciiuParameter, ocdeParameter)
                .ToList()
                        orderby r.nombre, r.ocde
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.ocde
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EventosCTI/EventosCTIOCDEACTI/yearSearch/catOCDE/catACTI
        //Lista los EventosCTI  en un año por categoria ocde y nivel acti
        public JsonResult EventosCTIOCDEACTI(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from r in db.Database
                .SqlQuery<eventosCTI_ocde_acti_Result>("eventosCTI_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.acti
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EventosCTI/EventosCTICIIUACTI/yearSearch/catCIIU/catACTI
        //Lista los EventosCTI  en un año por nivel ciiu y nivel acti
        public JsonResult EventosCTICIIUACTI(string yearSearch, string catCIIU, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<eventosCTI_ciiu_acti_Result>("eventosCTI_ciiu_acti @year, @ciiu, @acti", yearParameter, ciiuParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.acti
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /EventosCTI/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de EventosCTI por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI_área_ocde.rdlc");
                    break;
                case "2": //reporte de EventosCTI por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de EventosCTI por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de EventosCTI por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI_acti_n1.rdlc");
                    break;
                case "5": //reporte de EventosCTI por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI_acti_n2.rdlc");
                    break;
                case "6": //reporte de EventosCTI por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI_acti_n3.rdlc");
                    break;
                case "7": //reporte de EventosCTI por clase CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI_clase_ciiu.rdlc");
                    break;
                case "8": //reporte de EventosCTI por grupo CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI_grupo_ciiu.rdlc");
                    break;
                case "9": //reporte de EventosCTI por división CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI_división_ciiu.rdlc");
                    break;
                case "10": //reporte de EventosCTI por sección CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI_sección_ciiu.rdlc");
                    break;
                case "11": //reporte de EventosCTI por naturaleza del evento
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI_naturaleza_evento.rdlc");
                    break;
                default: //reporte de EventosCTI completo
                    path = Path.Combine(Server.MapPath("~/Reportes/EventosCTI"), "EventosCTI.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from r in db.Lista_EventosCTI
                        select new
                        {
                            Año = r.año,
                            r.Periodo,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Clase,
                            r.Grupo,
                            r.Division,
                            r.Seccion,
                            r.natEvento,
                            r.Cantidad
                        };
            }
            else
            {
                query = from r in db.Lista_EventosCTI.ToList()
                        where
                           r.año.ToString().CompareTo(desde) >= 0 &&
                           r.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = r.año,
                            r.Periodo,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Clase,
                            r.Grupo,
                            r.Division,
                            r.Seccion,
                            r.natEvento,
                            r.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}