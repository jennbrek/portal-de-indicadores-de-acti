﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class ServiciosCTController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /ServiciosCT/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /ServiciosCT/getYears
        //lista los años de registros de ServiciosCT
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_ServiciosCT
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ServiciosCT/Listar
        //lista todos los ServiciosCT
        public JsonResult Listar()
        {
            var query = from r in db.Lista_ServiciosCT
                        select new
                        {
                            Año = r.año,
                            Área_OCDE = r.Area,
                            Disciplina_OCDE = r.Disciplina,
                            Subárea_OCDE = r.Subarea,
                            ACTI_N1 = r.ACTI_N1,
                            ACTI_N2 = r.ACTI_N2,
                            ACTI_N3 = r.ACTI_N3,
                            Clase_CIIU = r.Clase,
                            Grupo_CIIU = r.Grupo,
                            División_CIIU = r.Division,
                            Sección_CIIU = r.Seccion,
                            Cantidad = r.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ServiciosCT/ListarxYear/tabla/desde/hasta
        //lista todos los ServiciosCT segun el tipo de tabla, el año desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from r in db.Database
                    .SqlQuery<serviciosCT_dimension_años_Result>("serviciosCT_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Subárea_OCDE = r.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from r in db.Database
                    .SqlQuery<serviciosCT_dimension_años_Result>("serviciosCT_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Disciplina_OCDE = r.nombre
                            };
                    break;
                case "4"://acti primer nivel
                    query = from r in db.Database
                    .SqlQuery<serviciosCT_dimension_años_Result>("serviciosCT_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N1 = r.nombre
                            };
                    break;
                case "5"://acti segundo nivel
                    query = from r in db.Database
                    .SqlQuery<serviciosCT_dimension_años_Result>("serviciosCT_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N2 = r.nombre
                            };
                    break;
                case "6"://acti tercer nivel
                    query = from r in db.Database
                    .SqlQuery<serviciosCT_dimension_años_Result>("serviciosCT_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                ACTI_N3 = r.nombre
                            };
                    break;
                case "7"://Clase CIIU
                    query = from r in db.Database
                    .SqlQuery<serviciosCT_dimension_años_Result>("serviciosCT_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Clase_CIIU = r.nombre
                            };
                    break;
                case "8"://Grupo CIIU
                    query = from r in db.Database
                    .SqlQuery<serviciosCT_dimension_años_Result>("serviciosCT_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Grupo_CIIU = r.nombre
                            };
                    break;
                case "9"://División CIIU
                    query = from r in db.Database
                    .SqlQuery<serviciosCT_dimension_años_Result>("serviciosCT_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                División_CIIU = r.nombre
                            };
                    break;
                case "10"://Sección CIIU
                    query = from r in db.Database
                    .SqlQuery<serviciosCT_dimension_años_Result>("serviciosCT_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Sección_CIIU = r.nombre
                            };
                    break;
                default://Área OCDE
                    query = from r in db.Database
                    .SqlQuery<serviciosCT_dimension_años_Result>("serviciosCT_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby r.año, r.nombre
                            select new
                            {
                                Año = r.año,
                                Cantidad = r.total,
                                Área_OCDE = r.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ServiciosCT/GraficoxYear/tabla/desde/hasta
        //lista todos los ServiciosCT segun el tipo de tabla, el año desde y hasta.
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from r in db.Database
                    .SqlQuery<serviciosCT_dimension_años_Result>("serviciosCT_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby r.año, r.nombre
                        select new
                        {
                            r.año,
                            r.total,
                            r.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ServiciosCT/ServiciosCTCIIUOCDE/yearSearch/catCIIU/catOCDE
        //Lista los ServiciosCT en un año por nivel ciiu y categoria ocde
        public JsonResult ServiciosCTCIIUOCDE(string yearSearch, string catCIIU, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from r in db.Database
                .SqlQuery<serviciosCT_ciiu_ocde_Result>("serviciosCT_ciiu_ocde @year, @ciiu, @ocde", yearParameter, ciiuParameter, ocdeParameter)
                .ToList()
                        orderby r.nombre, r.ocde
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.ocde
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ServiciosCT/ServiciosCTOCDEACTI/yearSearch/catOCDE/catACTI
        //Lista los ServiciosCT  en un año por categoria ocde y nivel acti
        public JsonResult ServiciosCTOCDEACTI(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from r in db.Database
                .SqlQuery<serviciosCT_ocde_acti_Result>("serviciosCT_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.acti
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ServiciosCT/ServiciosCTCIIUACTI/yearSearch/catCIIU/catACTI
        //Lista los ServiciosCT  en un año por nivel ciiu y nivel acti
        public JsonResult ServiciosCTCIIUACTI(string yearSearch, string catCIIU, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from r in db.Database
                .SqlQuery<serviciosCT_ciiu_acti_Result>("serviciosCT_ciiu_acti @year, @ciiu, @acti", yearParameter, ciiuParameter, actiParameter)
                .ToList()
                        orderby r.nombre, r.acti
                        select new
                        {
                            r.total,
                            r.nombre,
                            r.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /ServiciosCT/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de ServiciosCT por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/ServiciosCT"), "ServiciosCT_área_ocde.rdlc");
                    break;
                case "2": //reporte de ServiciosCT por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/ServiciosCT"), "ServiciosCT_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de ServiciosCT por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/ServiciosCT"), "ServiciosCT_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de ServiciosCT por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/ServiciosCT"), "ServiciosCT_acti_n1.rdlc");
                    break;
                case "5": //reporte de ServiciosCT por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/ServiciosCT"), "ServiciosCT_acti_n2.rdlc");
                    break;
                case "6": //reporte de ServiciosCT por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/ServiciosCT"), "ServiciosCT_acti_n3.rdlc");
                    break;
                case "7": //reporte de ServiciosCT por clase CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/ServiciosCT"), "ServiciosCT_clase_ciiu.rdlc");
                    break;
                case "8": //reporte de ServiciosCT por grupo CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/ServiciosCT"), "ServiciosCT_grupo_ciiu.rdlc");
                    break;
                case "9": //reporte de ServiciosCT por división CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/ServiciosCT"), "ServiciosCT_división_ciiu.rdlc");
                    break;
                case "10": //reporte de ServiciosCT por sección CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/ServiciosCT"), "ServiciosCT_sección_ciiu.rdlc");
                    break;
                default: //reporte de ServiciosCT completo
                    path = Path.Combine(Server.MapPath("~/Reportes/ServiciosCT"), "ServiciosCT.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from r in db.Lista_ServiciosCT
                        select new
                        {
                            Año = r.año,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Clase,
                            r.Grupo,
                            r.Division,
                            r.Seccion,
                            r.Cantidad
                        };
            }
            else
            {
                query = from r in db.Lista_ServiciosCT.ToList()
                        where
                           r.año.ToString().CompareTo(desde) >= 0 &&
                           r.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = r.año,
                            r.Area,
                            r.Subarea,
                            r.Disciplina,
                            r.ACTI_N1,
                            r.ACTI_N2,
                            r.ACTI_N3,
                            r.Clase,
                            r.Grupo,
                            r.Division,
                            r.Seccion,
                            r.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }

    }
}