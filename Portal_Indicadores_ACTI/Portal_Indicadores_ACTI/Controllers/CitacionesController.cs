﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class CitacionesController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /Citaciones/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Citaciones/getYears
        //lista los años de los registros de Citaciones
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_Citaciones
                         orderby year.Año
                         select new
                         {
                             año = year.Año
                         }).ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Citaciones/Listar
        //lista todos los Citaciones
        public JsonResult Listar()
        {
            var query = from x in db.Lista_Citaciones
                        select new
                        {
                            Categoría_Producto = x.CategoriaProd,
                            Naturaleza_Producto = x.NaturalezaProd,
                            Base_Datos_Bibliográfica = x.BD,
                            Disciplina_OCDE = x.Disciplina,
                            Año = x.Año,
                            Subárea_OCDE = x.Subarea,
                            Área_OCDE = x.Area,
                            Cantidad = x.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Citaciones/ListarxYear/tabla/desde/hasta
        //lista todos los Citaciones segun el tipo de tabla, el año (con periodo) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new System.Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2": //Subárea OCDE
                    query = from x in db.Database
                    .SqlQuery<citaciones_dimension_años_Result>("citaciones_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Subárea_OCDE = x.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from x in db.Database
                    .SqlQuery<citaciones_dimension_años_Result>("citaciones_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Disciplina_OCDE = x.nombre
                            };
                    break;
                case "4"://categoría del producto
                    query = from x in db.Database
                    .SqlQuery<citaciones_dimension_años_Result>("citaciones_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Categoría_Producto = x.nombre
                            };
                    break;
                case "5"://naturaleza producto
                    query = from x in db.Database
                    .SqlQuery<citaciones_dimension_años_Result>("citaciones_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Naturaleza_Producto = x.nombre
                            };
                    break;
                case "6"://bd
                    query = from x in db.Database
                    .SqlQuery<citaciones_dimension_años_Result>("citaciones_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Base_Datos_Bibliográfica = x.nombre
                            };
                    break;
                default://Área OCDE
                    query = from x in db.Database
                    .SqlQuery<citaciones_dimension_años_Result>("citaciones_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Área_OCDE = x.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Citaciones/GraficoxYear/tabla/desde/hasta
        //lista todos los Citaciones segun el tipo de tabla, el año (sin periodo) desde y hasta
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from x in db.Database
                    .SqlQuery<citaciones_dimension_años_Result>("citaciones_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby x.año, x.nombre
                        select new
                        {
                            x.año,
                            x.total,
                            x.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Citaciones/CitacionesProductoOCDE/yearSearch/catOCDE/catProducto
        //Lista los Citaciones para ACTI en un año por categoria ocde y producto
        public JsonResult CitacionesProductoOCDE(string yearSearch, string catOCDE, string catProducto)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var productoParameter = new SqlParameter("@producto", catProducto);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from x in db.Database
                .SqlQuery<citaciones_ocde_producto_Result>("citaciones_ocde_producto @year, @ocde, @producto", yearParameter, ocdeParameter, productoParameter)
                .ToList()
                        orderby x.nombre, x.producto
                        select new
                        {
                            x.total,
                            x.nombre,
                            x.producto
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Citaciones/CitacionesBDOCDE/yearSearch/catOCDE
        //Lista Citaciones de un año por la clasificacion ocde y bd
        public JsonResult CitacionesBDOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from x in db.Database
                .SqlQuery<citaciones_ocdeBD_Result>("Citaciones_ocdeBD @year,@ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.bd,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Citaciones/CitacionesBDProducto/yearSearch/catProducto
        //Lista Citaciones de un año por la clasificacion producto y bd
        public JsonResult CitacionesBDProducto(string yearSearch, string catProducto)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var productoParameter = new SqlParameter("@producto", catProducto);

            var query = from x in db.Database
                .SqlQuery<citaciones_productoBD_Result>("Citaciones_productoBD @year,@producto", yearParameter, productoParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.bd,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Citaciones/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de Citaciones por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Citaciones"), "Citaciones_área_ocde.rdlc");
                    break;
                case "2": //reporte de Citaciones por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Citaciones"), "Citaciones_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de Citaciones por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Citaciones"), "Citaciones_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de Citaciones por categoría del producto
                    path = Path.Combine(Server.MapPath("~/Reportes/Citaciones"), "Citaciones_categoría_producto.rdlc");
                    break;
                case "5": //reporte de Citaciones por naturaleza del producto
                    path = Path.Combine(Server.MapPath("~/Reportes/Citaciones"), "Citaciones_naturaleza_producto.rdlc");
                    break;
                case "6": //reporte de Citaciones por base de datos
                    path = Path.Combine(Server.MapPath("~/Reportes/Citaciones"), "Citaciones_bd_bibliográfica.rdlc");
                    break;
                default: //reporte de Citaciones completo
                    path = Path.Combine(Server.MapPath("~/Reportes/Citaciones"), "Citaciones.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from x in db.Lista_Citaciones
                        select new
                        {
                            x.Año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.BD,
                            x.CategoriaProd,
                            x.NaturalezaProd,
                            x.Cantidad
                        };
            }
            else
            {
                query = from x in db.Lista_Citaciones.ToList()
                        where
                           x.Año.ToString().CompareTo(desde) >= 0 &&
                           x.Año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            x.Año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.BD,
                            x.CategoriaProd,
                            x.NaturalezaProd,
                            x.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}