﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class IngresosProductosController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /IngresosProductos/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /IngresosProductos/getYears
        //lista los años de registros de ingresos por productos
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_IngresosProductos
                         orderby year.Año
                         select new
                         {
                             año = year.Año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosProductos/Listar
        //lista todos los ingresos por productos
        public JsonResult Listar()
        {
            var query = from i in db.Lista_IngresosProductos
                        select new
                        {
                            Año = i.Año,
                            Categoría_Producto = i.CatProd,
                            Naturaleza_Producto = i.NatProd,
                            Objeto_Contrato = i.ObjContrato,
                            Naturaleza_Organización = i.NatOrg,
                            Clase_CIIU = i.Clase,
                            Grupo_CIIU = i.Grupo,
                            División_CIIU = i.Division,
                            Sección_CIIU = i.Seccion,
                            Ámbito_Geográfico = i.AmbGeo,
                            Cantidad = i.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosProductos/ListarxYear/tabla/desde/hasta
        //lista todos los ingresos por productos segun el tipo de tabla, el año desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Naturaleza del producto
                    query = from i in db.Database
                    .SqlQuery<ingresosProductos_dimension_años_Result>("ingresosProductos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Naturaleza_Producto = i.nombre
                            };
                    break;
                case "3"://Objeto del contrato
                    query = from i in db.Database
                    .SqlQuery<ingresosProductos_dimension_años_Result>("ingresosProductos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Objeto_Contrato = i.nombre
                            };
                    break;
                case "4"://Naturaleza de la organización
                    query = from i in db.Database
                    .SqlQuery<ingresosProductos_dimension_años_Result>("ingresosProductos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Naturaleza_Organización = i.nombre
                            };
                    break;
                case "5"://Ámbito geográfico
                    query = from i in db.Database
                    .SqlQuery<ingresosProductos_dimension_años_Result>("ingresosProductos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Ámbito_Geográfico = i.nombre
                            };
                    break;
                case "6"://Clase CIIU
                    query = from i in db.Database
                    .SqlQuery<ingresosProductos_dimension_años_Result>("ingresosProductos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Clase_CIIU = i.nombre
                            };
                    break;
                case "7"://Grupo CIIU
                    query = from i in db.Database
                    .SqlQuery<ingresosProductos_dimension_años_Result>("ingresosProductos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Grupo_CIIU = i.nombre
                            };
                    break;
                case "8"://División CIIU
                    query = from i in db.Database
                    .SqlQuery<ingresosProductos_dimension_años_Result>("ingresosProductos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                División_CIIU = i.nombre
                            };
                    break;
                case "9"://Sección CIIU
                    query = from i in db.Database
                    .SqlQuery<ingresosProductos_dimension_años_Result>("ingresosProductos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Sección_CIIU = i.nombre
                            };
                    break;
                default://Categoría del producto
                    query = from i in db.Database
                    .SqlQuery<ingresosProductos_dimension_años_Result>("ingresosProductos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Categoría_Producto = i.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosProductos/GraficoxYear/tabla/desde/hasta
        //lista todos los ingresos por productos segun el tipo de tabla, el año desde y hasta para gráficos
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from i in db.Database
                    .SqlQuery<ingresosProductos_dimension_años_Result>("ingresosProductos_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby i.año, i.nombre
                        select new
                        {
                            i.año,
                            i.total,
                            i.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosProductos/IngresosProductosObjContrCIIU/yearSearch/catCIIU
        //Lista los ingresos por productos en un año por nivel ciiu y objeto del contrato
        public JsonResult IngresosProductosObjContrCIIU(string yearSearch, string catCIIU)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);

            var query = from i in db.Database
                .SqlQuery<ingresosProductos_ciiu_objContrato_Result>("ingresosProductos_ciiu_objContrato @year, @ciiu", yearParameter, ciiuParameter)
                .ToList()
                        orderby i.nombre, i.objeto
                        select new
                        {
                            i.total,
                            i.nombre,
                            i.objeto
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosProductos/IngresosProductosNatProdCIIU/yearSearch/catCIIU/catProducto
        //Lista los ingresos por productos en un año por nivel ciiu y la naturaleza del producto
        public JsonResult IngresosProductosNatProdCIIU(string yearSearch, string catCIIU, string catProducto)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var productoParameter = new SqlParameter("@producto", catProducto);

            var query = from i in db.Database
                .SqlQuery<ingresosProductos_ciiu_producto_Result>("ingresosProductos_ciiu_producto @year, @ciiu, @producto", yearParameter, ciiuParameter, productoParameter)
                .ToList()
                        orderby i.nombre, i.producto
                        select new
                        {
                            i.total,
                            i.nombre,
                            i.producto
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosProductos/IngresosProductosNatOrgCIIU/yearSearch/catCIIU
        //Lista los ingresos por productos en un año por nivel ciiu y naturaleza de la organización
        public JsonResult IngresosProductosNatOrgCIIU(string yearSearch, string catCIIU)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);

            var query = from i in db.Database
                .SqlQuery<ingresosProductos_ciiu_natOrg_Result>("ingresosProductos_ciiu_natOrg @year, @ciiu", yearParameter, ciiuParameter)
                .ToList()
                        orderby i.nombre, i.organizacion
                        select new
                        {
                            i.total,
                            i.nombre,
                            i.organizacion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosProductos/IngresosProductosAmbGeoCIIU/yearSearch/catCIIU
        //Lista los ingresos por productos en un año por nivel ciiu y ámbito geográfico
        public JsonResult IngresosProductosAmbGeoCIIU(string yearSearch, string catCIIU)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);

            var query = from i in db.Database
                .SqlQuery<ingresosProductos_ciiu_ambGeo_Result>("ingresosProductos_ciiu_ambGeo @year, @ciiu", yearParameter, ciiuParameter)
                .ToList()
                        orderby i.nombre, i.ambito
                        select new
                        {
                            i.total,
                            i.nombre,
                            i.ambito
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosProductosCTI/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de IngresosProductosCTI por categoría del producto
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosProductosCTI"), "IngresosProductosCTI_categoría_producto.rdlc");
                    break;
                case "2": //reporte de IngresosProductosCTI por naturaleza del producto
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosProductosCTI"), "IngresosProductosCTI_naturaleza_producto.rdlc");
                    break;
                case "3": //reporte de IngresosProductosCTI por objeto del contrato
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosProductosCTI"), "IngresosProductosCTI_objeto_contrato.rdlc");
                    break;
                case "4": //reporte de IngresosProductosCTI por naturaleza de la organización
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosProductosCTI"), "IngresosProductosCTI_naturaleza_organización.rdlc");
                    break;
                case "5": //reporte de IngresosProductosCTI por ámbito geográfico
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosProductosCTI"), "IngresosProductosCTI_ámbito_geográfico.rdlc");
                    break;
                case "6": //reporte de IngresosProductosCTI por clase CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosProductosCTI"), "IngresosProductosCTI_clase_ciiu.rdlc");
                    break;
                case "7": //reporte de IngresosProductosCTI por grupo CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosProductosCTI"), "IngresosProductosCTI_grupo_ciiu.rdlc");
                    break;
                case "8": //reporte de IngresosProductosCTI por división CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosProductosCTI"), "IngresosProductosCTI_división_ciiu.rdlc");
                    break;
                case "9": //reporte de IngresosProductosCTI por sección CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosProductosCTI"), "IngresosProductosCTI_sección_ciiu.rdlc");
                    break;
                default: //reporte de IngresosProductosCTI completo
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosProductosCTI"), "IngresosProductosCTI.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from i in db.Lista_IngresosProductos
                        select new
                        {
                            Año = i.Año,
                            CategoriaProd = i.CatProd,
                            NaturalezaProd = i.NatProd,
                            ObjetoContrato = i.ObjContrato,
                            NaturalezaOrg = i.NatOrg,
                            ClaseCIIU = i.Clase,
                            GrupoCIIU = i.Grupo,
                            DivisionesCIIU = i.Division,
                            SeccionesCIIU = i.Seccion,
                            AmbitoGeo = i.AmbGeo,
                            Ingresos = i.Cantidad
                        };
            }
            else
            {
                query = from i in db.Lista_IngresosProductos.ToList()
                        where
                           i.Año.ToString().CompareTo(desde) >= 0 &&
                           i.Año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = i.Año,
                            CategoriaProd = i.CatProd,
                            NaturalezaProd = i.NatProd,
                            ObjetoContrato = i.ObjContrato,
                            NaturalezaOrg = i.NatOrg,
                            ClaseCIIU = i.Clase,
                            GrupoCIIU = i.Grupo,
                            DivisionesCIIU = i.Division,
                            SeccionesCIIU = i.Seccion,
                            AmbitoGeo = i.AmbGeo,
                            Ingresos = i.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}