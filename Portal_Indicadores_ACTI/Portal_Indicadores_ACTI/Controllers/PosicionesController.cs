﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class PosicionesController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /Posiciones/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Posiciones/getYears
        //lista los años de los registros de Posiciones
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_Posiciones
                         orderby year.año
                         select new
                         {
                             año = year.año
                         }).ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Posiciones/Listar/desde/hasta
        //lista todos los Posiciones por rango de años
        public JsonResult Listar(string desde, string hasta)
        {
            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from x in db.Lista_Posiciones
                        select new
                        {
                            Ámbito_Geográfico = x.Ambito,
                            Ranking = x.Ranking,
                            Disciplina_OCDE = x.Disciplina,
                            Año = x.año,
                            Subárea_OCDE = x.Subarea,
                            Área_OCDE = x.Area,
                            Posición = x.Cantidad
                        };
            }
            else
            {
                query = from x in db.Lista_Posiciones.ToList()
                        where
                           x.año.ToString().CompareTo(desde) >= 0 &&
                           x.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Ámbito_Geográfico = x.Ambito,
                            Ranking = x.Ranking,
                            Disciplina_OCDE = x.Disciplina,
                            Año = x.año,
                            Subárea_OCDE = x.Subarea,
                            Área_OCDE = x.Area,
                            Posición = x.Cantidad
                        };
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Posiciones/Reporte/id/desde/hasta/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reportes/Posiciones"), "Posiciones.rdlc");

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from x in db.Lista_Posiciones
                        select new
                        {
                            Año = x.año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.Ambito,
                            x.Ranking,
                            x.Cantidad
                        };
            }
            else
            {
                query = from x in db.Lista_Posiciones.ToList()
                        where
                           x.año.ToString().CompareTo(desde) >= 0 &&
                           x.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = x.año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.Ambito,
                            x.Ranking,
                            x.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}