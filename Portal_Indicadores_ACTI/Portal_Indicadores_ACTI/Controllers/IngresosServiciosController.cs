﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;
namespace Portal_Indicadores_ACTI.Controllers
{
    public class IngresosServiciosController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /IngresosServicios/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /IngresosServicios/getYears
        //lista los años de registros de ingresos por servicios
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_IngresosServicios
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosServicios/Listar
        //lista todos los ingresos por servicios
        public JsonResult Listar()
        {
            var query = from i in db.Lista_IngresosServicios
                        select new
                        {
                            Año = i.año,
                            Objeto_Contrato = i.Objeto,
                            Naturaleza_Organización = i.NaturalezaOrg,
                            Clase_CIIU = i.Clase,
                            Grupo_CIIU = i.Grupo,
                            División_CIIU = i.Division,
                            Sección_CIIU = i.Seccion,
                            Ámbito_Geográfico = i.Ambito,
                            ACTI_N1 = i.ACTI_N1,
                            ACTI_N2 = i.ACTI_N2,
                            ACTI_N3 = i.ACTI_N3,
                            Cantidad = i.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosServicios/ListarxYear/tabla/desde/hasta
        //lista todos los ingresos por servicios segun el tipo de tabla, el año desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://acti nivel 2
                    query = from i in db.Database
                    .SqlQuery<ingresosServicios_dimension_años_Result>("ingresosServicios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                ACTI_N2 = i.nombre
                            };
                    break;
                case "3"://acti nivel 3
                    query = from i in db.Database
                    .SqlQuery<ingresosServicios_dimension_años_Result>("ingresosServicios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                ACTI_N3 = i.nombre
                            };
                    break;
                case "4"://Objeto del contrato
                    query = from i in db.Database
                    .SqlQuery<ingresosServicios_dimension_años_Result>("ingresosServicios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Objeto_Contrato = i.nombre
                            };
                    break;
                case "5"://Naturaleza de la organización
                    query = from i in db.Database
                    .SqlQuery<ingresosServicios_dimension_años_Result>("ingresosServicios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Naturaleza_Organización = i.nombre
                            };
                    break;
                case "6"://Ámbito geográfico
                    query = from i in db.Database
                    .SqlQuery<ingresosServicios_dimension_años_Result>("ingresosServicios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Ámbito_Geográfico = i.nombre
                            };
                    break;
                case "7"://Clase CIIU
                    query = from i in db.Database
                    .SqlQuery<ingresosServicios_dimension_años_Result>("ingresosServicios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Clase_CIIU = i.nombre
                            };
                    break;
                case "8"://Grupo CIIU
                    query = from i in db.Database
                    .SqlQuery<ingresosServicios_dimension_años_Result>("ingresosServicios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Grupo_CIIU = i.nombre
                            };
                    break;
                case "9"://División CIIU
                    query = from i in db.Database
                    .SqlQuery<ingresosServicios_dimension_años_Result>("ingresosServicios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                División_CIIU = i.nombre
                            };
                    break;
                case "10"://Sección CIIU
                    query = from i in db.Database
                    .SqlQuery<ingresosServicios_dimension_años_Result>("ingresosServicios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                Sección_CIIU = i.nombre
                            };
                    break;
                default://acti nivel 1
                    query = from i in db.Database
                    .SqlQuery<ingresosServicios_dimension_años_Result>("ingresosServicios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby i.año, i.nombre
                            select new
                            {
                                Año = i.año,
                                Cantidad = i.total,
                                ACTI_N1 = i.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosServicios/GraficoxYear/tabla/desde/hasta
        //lista todos los ingresos por servicios segun el tipo de tabla, el año desde y hasta para gráficos
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from i in db.Database
                    .SqlQuery<ingresosServicios_dimension_años_Result>("ingresosServicios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby i.año, i.nombre
                        select new
                        {
                            i.año,
                            i.total,
                            i.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosServicios/IngresosServiciosNatOrgACTI/yearSearch/catACTI
        //Lista los ingresos por servicios en un año por nivel acti y naturaleza de la organización
        public JsonResult IngresosServiciosNatOrgACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from i in db.Database
                .SqlQuery<ingresosServicios_acti_natOrg_Result>("ingresosServicios_acti_natOrg @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby i.nombre, i.organizacion
                        select new
                        {
                            i.total,
                            i.nombre,
                            i.organizacion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosServicios/IngresosServiciosObjContrACTI/yearSearch/catACTI
        //Lista los ingresos por servicios en un año por nivel acti y objeto del contrato
        public JsonResult IngresosServiciosObjContrACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from i in db.Database
                .SqlQuery<ingresosServicios_acti_objContrato_Result>("ingresosServicios_acti_objContrato @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby i.nombre, i.objeto
                        select new
                        {
                            i.total,
                            i.nombre,
                            i.objeto
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosServicios/IngresosServiciosAmbGeoACTI/yearSearch/catACTI
        //Lista los ingresos por servicios en un año por nivel acti y ámbito geográfico
        public JsonResult IngresosServiciosAmbGeoACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from i in db.Database
                .SqlQuery<ingresosServicios_acti_ambGeo_Result>("ingresosServicios_acti_ambGeo @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby i.nombre, i.ambito
                        select new
                        {
                            i.total,
                            i.nombre,
                            i.ambito
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosServicios/IngresosServiciosCIIUACTI/yearSearch/catCIIU/catACTI
        //Lista los IngresosServicios para ACTI en un año por nivel ciiu y nivel acti
        public JsonResult IngresosServiciosCIIUACTI(string yearSearch, string catCIIU, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from i in db.Database
                .SqlQuery<ingresosServicios_ciiu_acti_Result>("ingresosServicios_ciiu_acti @year, @ciiu, @acti", yearParameter, ciiuParameter, actiParameter)
                .ToList()
                        orderby i.nombre, i.acti
                        select new
                        {
                            i.total,
                            i.nombre,
                            i.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /IngresosServicios/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de IngresosServicios por primer nivel acti
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosServicios"), "IngresosServicios_acti_n1.rdlc");
                    break;
                case "2": //reporte de IngresosServicios por segundo nivel acti
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosServicios"), "IngresosServicios_acti_n2.rdlc");
                    break;
                case "3": //reporte de IngresosServicios por tercer nivel acti
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosServicios"), "IngresosServicios_acti_n3.rdlc");
                    break;
                case "4": //reporte de IngresosServicios por objeto del contrato
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosServicios"), "IngresosServicios_objeto_contrato.rdlc");
                    break;
                case "5": //reporte de IngresosServicios por naturaleza de la organización
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosServicios"), "IngresosServicios_naturaleza_organización.rdlc");
                    break;
                case "6": //reporte de IngresosServicios por ámbito geográfico
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosServicios"), "IngresosServicios_ámbito_geográfico.rdlc");
                    break;
                case "7": //reporte de IngresosServicios por clase CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosServicios"), "IngresosServicios_clase_ciiu.rdlc");
                    break;
                case "8": //reporte de IngresosServicios por grupo CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosServicios"), "IngresosServicios_grupo_ciiu.rdlc");
                    break;
                case "9": //reporte de IngresosServicios por división CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosServicios"), "IngresosServicios_división_ciiu.rdlc");
                    break;
                case "10": //reporte de IngresosServicios por sección CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosServicios"), "IngresosServicios_sección_ciiu.rdlc");
                    break;
                default: //reporte de IngresosServicios completo
                    path = Path.Combine(Server.MapPath("~/Reportes/IngresosServicios"), "IngresosServicios.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from i in db.Lista_IngresosServicios
                        select new
                        {
                            Año = i.año,
                            i.ACTI_N1,
                            i.ACTI_N2,
                            i.ACTI_N3,
                            i.Objeto,
                            i.NaturalezaOrg,
                            i.Clase,
                            i.Grupo,
                            i.Division,
                            i.Seccion,
                            i.Ambito,
                            i.Cantidad
                        };
            }
            else
            {
                query = from i in db.Lista_IngresosServicios.ToList()
                        where
                           i.año.ToString().CompareTo(desde) >= 0 &&
                           i.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Año = i.año,
                            i.ACTI_N1,
                            i.ACTI_N2,
                            i.ACTI_N3,
                            i.Objeto,
                            i.NaturalezaOrg,
                            i.Clase,
                            i.Grupo,
                            i.Division,
                            i.Seccion,
                            i.Ambito,
                            i.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }

    }
}