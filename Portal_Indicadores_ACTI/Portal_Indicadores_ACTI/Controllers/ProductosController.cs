﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class ProductosController : Controller
    {

        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        //
        // GET: /Productos/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Productos/getYears
        //lista los años de registros de producto
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_Producto
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Productos/Listar
        //lista todos los productos
        public JsonResult Listar()
        {
            var query = from x in db.Lista_Producto
                        select new
                        {
                            Año = x.año,
                            Área_OCDE = x.Area,
                            Disciplina_OCDE = x.Disciplina,
                            Subárea_OCDE = x.Subarea,
                            ACTI_N1 = x.ACTI_N1,
                            ACTI_N2 = x.ACTI_N2,
                            ACTI_N3 = x.ACTI_N3,
                            Clase_CIIU = x.Clase,
                            Grupo_CIIU = x.Grupo,
                            División_CIIU = x.Division,
                            Sección_CIIU = x.Seccion,
                            Ámbito_Geográfico = x.Ambito,
                            Naturaleza_Producto = x.NaturalezaProd,
                            Categoría_Producto = x.CategoriaProducto,
                            Tipología_COLCIENCIAS = x.Colciencias,
                            Idioma = x.Idioma,
                            Elaboración_Colaborativa = x.Colaboracion,
                            Citaciones = x.citado,
                            Cantidad = x.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Productos/ListarxYear/tabla/acti/desde/hasta
        //lista todos los productos segun el tipo de tabla, la acti, el año desde y hasta
        public JsonResult ListarxYear(string tabla, string acti, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var actiParameter = new SqlParameter("@acti", acti);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Subárea_OCDE = x.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Disciplina_OCDE = x.nombre
                            };
                    break;
                case "4"://acti segundo nivel
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                ACTI_N2 = x.nombre
                            };
                    break;
                case "5"://acti tercer nivel
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                ACTI_N3 = x.nombre
                            };
                    break;
                case "6"://clase ciiu
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Clase_CIIU = x.nombre
                            };
                    break;
                case "7"://grupo ciiu
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Grupo_CIIU = x.nombre
                            };
                    break;
                case "8"://división ciiu
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                División_CIIU = x.nombre
                            };
                    break;
                case "9"://sección ciiu
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Sección_CIIU = x.nombre
                            };
                    break;
                case "10"://ámbito geográfico
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Ámbito_Geográfico = x.nombre
                            };
                    break;
                case "11"://naturaleza del producto
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Naturaleza_Producto = x.nombre
                            };
                    break;
                case "12"://categoría del producto
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Categoría_Producto = x.nombre
                            };
                    break;
                case "13"://tipología colciencias
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Tipología_COLCIENCIAS = x.nombre
                            };
                    break;
                case "14"://idioma
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Idioma = x.nombre
                            };
                    break;
                case "15"://elaboración colaborativa
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Elaboración_Colaborativa = x.nombre
                            };
                    break;
                case "16"://citaciones
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Citaciones = x.nombre
                            };
                    break;
                default://Área OCDE
                    query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                Área_OCDE = x.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Productos/GraficoxYear/tabla/acti/desde/hasta
        //lista todos los productos segun el tipo de tabla, la acti, el año desde y hasta.
        public JsonResult GraficoxYear(string tabla, string acti, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var actiParameter = new SqlParameter("@acti", acti);

            var query = from x in db.Database
                    .SqlQuery<producto_dimension_años_Result>("producto_dimension_años @yearDesde,@yearHasta,@dimension,@acti", yearDesdeParameter, yearHastaParameter, dimensionParameter, actiParameter)
                    .ToList()
                        orderby x.año, x.nombre
                        select new
                        {
                            x.año,
                            x.total,
                            x.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Productos/ProductosCIIUOCDE/yearSearch/catCIIU/catOCDE/acti1
        //Lista los productos en un año por nivel ciiu, categoría ocde y acti primer nivel
        public JsonResult ProductosCIIUOCDE(string yearSearch, string catCIIU, string catOCDE, string acti1)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);
            var acti1Parameter = new SqlParameter("@acti1", acti1);

            var query = from x in db.Database
                .SqlQuery<producto_ciiu_ocde_Result>("producto_ciiu_ocde @year, @ciiu, @ocde, @acti1", yearParameter, ciiuParameter, ocdeParameter, acti1Parameter)
                .ToList()
                        orderby x.nombre, x.ocde
                        select new
                        {
                            x.total,
                            x.nombre,
                            x.ocde
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Productos/ProductosCIIUProducto/yearSearch/catCIIU/catProducto/acti1
        //Lista los productos en un año por nivel ciiu, categoría producto y acti primer nivel
        public JsonResult ProductosCIIUProducto(string yearSearch, string catCIIU, string catProducto, string acti1)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ciiuParameter = new SqlParameter("@ciiu", catCIIU);
            var productoParameter = new SqlParameter("@producto", catProducto);
            var acti1Parameter = new SqlParameter("@acti1", acti1);

            var query = from x in db.Database
                .SqlQuery<producto_ciiu_producto_Result>("producto_ciiu_producto @year, @ciiu, @producto, @acti1", yearParameter, ciiuParameter, productoParameter, acti1Parameter)
                .ToList()
                        orderby x.nombre, x.producto
                        select new
                        {
                            x.total,
                            x.nombre,
                            x.producto
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Productos/ProductosOCDEACTI/yearSearch/catOCDE/catProducto/acti1
        //Lista los productos en un año por categoria ocde, categoría producto y acti1
        public JsonResult ProductosOCDEProducto(string yearSearch, string catOCDE, string catProducto, string acti1)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var productoParameter = new SqlParameter("@producto", catProducto);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);
            var acti1Parameter = new SqlParameter("@acti1", acti1);

            var query = from x in db.Database
                .SqlQuery<producto_ocde_producto_Result>("producto_ocde_producto @year, @ocde, @producto, @acti1", yearParameter, ocdeParameter, productoParameter, acti1Parameter)
                .ToList()
                        orderby x.nombre, x.producto
                        select new
                        {
                            x.total,
                            x.nombre,
                            x.producto
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Productos/Reporte/id/desde/hasta/tipoReporte/fileName/acti
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string acti, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de Productos por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_área_ocde.rdlc");
                    break;
                case "2": //reporte de Productos por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de Productos por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de Productos por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_acti_n2.rdlc");
                    break;
                case "5": //reporte de Productos por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_acti_n3.rdlc");
                    break;
                case "6": //reporte de Productos por clase CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_clase_ciiu.rdlc");
                    break;
                case "7": //reporte de Productos por grupo CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_grupo_ciiu.rdlc");
                    break;
                case "8": //reporte de Productos por división CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_división_ciiu.rdlc");
                    break;
                case "9": //reporte de Productos por sección CIIU
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_sección_ciiu.rdlc");
                    break;
                case "10": //reporte de Productos por ámbito geográfico
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_ámbito_geográfico.rdlc");
                    break;
                case "11": //reporte de Productos por naturaleza del producto
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_naturaleza_producto.rdlc");
                    break;
                case "12": //reporte de Productos por categoría del producto
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_categoría_producto.rdlc");
                    break;
                case "13": //reporte de Productos por tipología colciencias
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_tipología_colciencias.rdlc");
                    break;
                case "14": //reporte de Productos por idioma
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_idioma.rdlc");
                    break;
                case "15": //reporte de Productos por elaboración colaborativa
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_elaboración_colaborativa.rdlc");
                    break;
                case "16": //reporte de Productos por citaciones
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos_citaciones.rdlc");
                    break;
                default: //reporte de Productos completo
                    path = Path.Combine(Server.MapPath("~/Reportes/Productos"), "Productos.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from x in db.Lista_Producto.ToList()
                        where x.ACTI_N1.ToString().Equals(acti)
                        select new
                        {
                            Año = x.año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            x.Clase,
                            x.Grupo,
                            x.Division,
                            x.Seccion,
                            x.Ambito,
                            x.CategoriaProducto,
                            x.NaturalezaProd,
                            x.citado,
                            x.Idioma,
                            x.Colaboracion,
                            x.Cantidad
                        };
            }
            else
            {
                query = from x in db.Lista_Producto.ToList()
                        where
                           x.año.ToString().CompareTo(desde) >= 0 &&
                           x.año.ToString().CompareTo(hasta) <= 0 &&
                           x.ACTI_N1.ToString().Equals(acti)
                        select new
                        {
                            Año = x.año,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            x.Clase,
                            x.Grupo,
                            x.Division,
                            x.Seccion,
                            x.Ambito,
                            x.CategoriaProducto,
                            x.NaturalezaProd,
                            x.citado,
                            x.Idioma,
                            x.Colaboracion,
                            x.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }

    }
}