﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class EstudiantesController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 88675309 // Usa este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /Estudiantes/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Estudiantes/getYears
        //lista los años de registros de estudiantes
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_Estudiantes
                         orderby year.Año
                         select new
                         {
                             año = year.Año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Estudiantes/Listar
        //lista todos los estudiantes
        public JsonResult Listar()
        {
            var query = from e in db.Lista_Estudiantes
                        select new
                        {
                            Año = e.Año,
                            Periodo = e.Periodo,
                            Cantidad = e.Cantidad,
                            Área_OCDE = e.Area,
                            Subárea_OCDE = e.Subarea,
                            Disciplina_OCDE = e.Disciplina,
                            Fuente_Financiación = e.Financiacion,
                            Nivel_Formación = e.Formacion,
                            Sexo = e.Sexo,
                            Estrato = e.Estrato,
                            País = e.Pais,
                            Departamento_Estado = e.Departamento,
                            Ciudad = e.Ciudad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Estudiantes/ListarxYear/tabla/desde/hasta
        //lista todos los estudiantes segun el tipo de tabla, el año (con periodo) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from e in db.Database
                    .SqlQuery<estudiantes_dimension_Result>("estudiantes_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.semestre, e.nombre
                            select new
                            {
                                Año = e.año,
                                Periodo = e.semestre,
                                Cantidad = e.total,
                                Subárea_OCDE = e.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from e in db.Database
                    .SqlQuery<estudiantes_dimension_Result>("estudiantes_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.semestre, e.nombre
                            select new
                            {
                                Año = e.año,
                                Periodo = e.semestre,
                                Cantidad = e.total,
                                Disciplina_OCDE = e.nombre
                            };
                    break;
                case "4"://Nivel de formación
                    query = from e in db.Database
                    .SqlQuery<estudiantes_dimension_Result>("estudiantes_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.semestre, e.nombre
                            select new
                            {
                                Año = e.año,
                                Periodo = e.semestre,
                                Cantidad = e.total,
                                Nivel_Formación = e.nombre
                            };
                    break;
                case "5"://Fuente de financiación
                    query = from e in db.Database
                    .SqlQuery<estudiantes_dimension_Result>("estudiantes_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.semestre, e.nombre
                            select new
                            {
                                Año = e.año,
                                Periodo = e.semestre,
                                Cantidad = e.total,
                                Fuente_Financiación = e.nombre
                            };
                    break;
                case "6"://Estrato socioeconómico
                    query = from e in db.Database
                    .SqlQuery<estudiantes_dimension_Result>("estudiantes_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.semestre, e.nombre
                            select new
                            {
                                Año = e.año,
                                Periodo = e.semestre,
                                Cantidad = e.total,
                                Estrato = e.nombre
                            };
                    break;
                case "7"://Sexo
                    query = from e in db.Database
                    .SqlQuery<estudiantes_dimension_Result>("estudiantes_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.semestre, e.nombre
                            select new
                            {
                                Año = e.año,
                                Periodo = e.semestre,
                                Cantidad = e.total,
                                Sexo = e.nombre
                            };
                    break;
                case "8"://País
                    query = from e in db.Database
                    .SqlQuery<estudiantes_dimension_Result>("estudiantes_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.semestre, e.nombre
                            select new
                            {
                                Año = e.año,
                                Periodo = e.semestre,
                                Cantidad = e.total,
                                País = e.nombre
                            };
                    break;
                case "9"://Departamento o estado
                    query = from e in db.Database
                    .SqlQuery<estudiantes_dimension_Result>("estudiantes_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.semestre, e.nombre
                            select new
                            {
                                Año = e.año,
                                Periodo = e.semestre,
                                Cantidad = e.total,
                                Departamento_Estado = e.nombre
                            };
                    break;
                case "10"://Ciudad
                    query = from e in db.Database
                    .SqlQuery<estudiantes_dimension_Result>("estudiantes_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.semestre, e.nombre
                            select new
                            {
                                Año = e.año,
                                Periodo = e.semestre,
                                Cantidad = e.total,
                                Ciudad = e.nombre
                            };
                    break;
                default://Área ocde
                    query = from e in db.Database
                    .SqlQuery<estudiantes_dimension_Result>("estudiantes_dimension @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.semestre, e.nombre
                            select new
                            {
                                Año = e.año,
                                Periodo = e.semestre,
                                Cantidad = e.total,
                                Área_OCDE = e.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Estudiantes/GraficoxYear/tabla/desde/hasta
        //lista todos los estudiantes segun el tipo de tabla, el año (sin periodo) desde y hasta
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from e in db.Database
                    .SqlQuery<estudiantes_dimension_años_Result>("estudiantes_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby e.año, e.nombre
                        select new
                        {
                            e.año,
                            e.total,
                            e.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Estudiantes/EstsYearFormacionOCDE/yearSearch/catOCDE
        //Lista estudiantes de un año por nivel OCDE y nivel de formacion
        public JsonResult EstsYearFormacionOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from e in db.Database
                .SqlQuery<estudiantes_ocde_formacion_Result>("estudiantes_ocde_formacion @year,@ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby e.formacion, e.nombre
                        select new
                        {
                            e.total,
                            e.formacion,
                            e.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Estudiantes/EstsYearFinanciacionOCDE/yearSearch/catOCDE
        //Lista estudiantes de un año por nivel OCDE y fuente de financiación
        public JsonResult EstsYearFinanciacionOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from e in db.Database
                .SqlQuery<estudiantes_ocde_financiacion_Result>("estudiantes_ocde_financiacion @year,@ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby e.financiacion, e.nombre
                        select new
                        {
                            e.total,
                            e.financiacion,
                            e.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Estudiantes/EstsYearSexoOCDE/yearSearch/catOCDE
        //Lista estudiantes de un año por nivel OCDE y sexo
        public JsonResult EstsYearSexoOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from e in db.Database
                 .SqlQuery<estudiantes_ocde_sexo_Result>("estudiantes_ocde_sexo @year,@ocde", yearParameter, ocdeParameter)
                 .ToList()
                        orderby e.sexo, e.nombre
                        select new
                        {
                            e.total,
                            e.sexo,
                            e.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Estudiantes/EstsYearEstratoOCDE/yearSearch/catOCDE
        //Lista estudiantes de un año por nivel OCDE y estrato
        public JsonResult EstsYearEstratoOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from e in db.Database
                .SqlQuery<estudiantes_ocde_estrato_Result>("estudiantes_ocde_estrato @year,@ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby e.estrato, e.nombre
                        select new
                        {
                            e.total,
                            e.estrato,
                            e.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Estudiantes/EstsYearProcedenciaOCDE/yearSearch/catOCDE/tipoProcedencia
        //Lista estudiantes de un año por nivel OCDE y procedencia
        public JsonResult EstsYearProcedenciaOCDE(string yearSearch, string catOCDE, string tipoProcedencia)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);
            var procedenciaParameter = new SqlParameter("@procedencia", tipoProcedencia);

            var query = from e in db.Database
                 .SqlQuery<estudiantes_ocde_procedencia_Result>("estudiantes_ocde_procedencia @year,@ocde, @procedencia", yearParameter, ocdeParameter, procedenciaParameter)
                 .ToList()
                        orderby e.procedencia, e.nombre
                        select new
                        {
                            e.total,
                            e.procedencia,
                            e.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /Estudiantes/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de estudiantes por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Estudiantes"), "Estudiantes_área_ocde.rdlc");
                    break;
                case "2": //reporte de estudiantes por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Estudiantes"), "Estudiantes_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de estudiantes por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/Estudiantes"), "Estudiantes_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de Estudiantes por fuente de financiación
                    path = Path.Combine(Server.MapPath("~/Reportes/Estudiantes"), "Estudiantes_financiación.rdlc");
                    break;
                case "5": //reporte de Estudiantes por nivel de formación
                    path = Path.Combine(Server.MapPath("~/Reportes/Estudiantes"), "Estudiantes_formación.rdlc");
                    break;
                case "6": //reporte de estudiantes por estrato
                    path = Path.Combine(Server.MapPath("~/Reportes/Estudiantes"), "Estudiantes_estrato.rdlc");
                    break;
                case "7": //reporte de Estudiantes por sexo
                    path = Path.Combine(Server.MapPath("~/Reportes/Estudiantes"), "Estudiantes_sexo.rdlc");
                    break;
                case "8": //reporte de Estudiantes por país de procedencia
                    path = Path.Combine(Server.MapPath("~/Reportes/Estudiantes"), "Estudiantes_país.rdlc");
                    break;
                case "9": //reporte de Estudiantes por estado o departamento de procedencia
                    path = Path.Combine(Server.MapPath("~/Reportes/Estudiantes"), "Estudiantes_departamento-estado.rdlc");
                    break;
                case "10": //reporte de Estudiantes por ciudad de procedencia
                    path = Path.Combine(Server.MapPath("~/Reportes/Estudiantes"), "Estudiantes_ciudad.rdlc");
                    break;
                default: //reporte de Estudiantes completo
                    path = Path.Combine(Server.MapPath("~/Reportes/Estudiantes"), "Estudiantes.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from e in db.Lista_Estudiantes
                        select new
                        {
                            Nivel = e.Formacion,
                            Fuente = e.Financiacion,
                            Disciplina = e.Disciplina,
                            Subarea = e.Subarea,
                            Area = e.Area,
                            Año = e.Año,
                            Periodo = e.Periodo,
                            Sexo = e.Sexo,
                            Estrato = e.Estrato,
                            Pais = e.Pais,
                            Departamento = e.Departamento,
                            Ciudad = e.Ciudad,
                            Estudiantes = e.Cantidad
                        };
            }
            else
            {
                query = from e in db.Lista_Estudiantes.ToList()
                        where
                           e.Año.ToString().CompareTo(desde) >= 0 &&
                           e.Año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Nivel = e.Formacion,
                            Fuente = e.Financiacion,
                            Disciplina = e.Disciplina,
                            Subarea = e.Subarea,
                            Area = e.Area,
                            Año = e.Año,
                            Periodo = e.Periodo,
                            Sexo = e.Sexo,
                            Estrato = e.Estrato,
                            Pais = e.Pais,
                            Departamento = e.Departamento,
                            Ciudad = e.Ciudad,
                            Estudiantes = e.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }

    }
}