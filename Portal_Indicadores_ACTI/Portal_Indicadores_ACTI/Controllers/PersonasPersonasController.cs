﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class PersonasPersonasController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /PersonasPersonas/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /PersonasPersonas/getYears
        //lista los años de los registros de PersonasPersonas
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_PersonasPersonas
                         orderby year.Año
                         select new
                         {
                             año = year.Año
                         }).ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PersonasPersonas/Listar
        //lista todos las PersonasPersonas
        public JsonResult Listar()
        {
            var query = from x in db.Lista_PersonasPersonas
                        select new
                        {
                            Sexo = x.Sexo,
                            Rango_Edad = x.Edad,
                            Nacionalidad = x.Nacionalidad,
                            Nivel_Formación = x.Formacion,
                            Año = x.Año,
                            Periodo = x.Periodo,
                            Tipo_Vinculación = x.Vinculo,
                            Cantidad = x.Cantidad,
                            CantidadTE = x.CantidadTE
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PersonasPersonas/ListarxYear/tabla/desde/hasta
        //lista todos los PersonasPersonas segun el tipo de tabla, el año (con periodo) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new System.Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2": //Rango de edad
                    query = from x in db.Database
                    .SqlQuery<personasPersonas_dimension_años_Result>("personasPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Rango_Edad = x.nombre
                            };
                    break;
                case "3"://Nivel de formación
                    query = from x in db.Database
                    .SqlQuery<personasPersonas_dimension_años_Result>("personasPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Nivel_Formación = x.nombre
                            };
                    break;
                case "4"://Nacionalidad
                    query = from x in db.Database
                    .SqlQuery<personasPersonas_dimension_años_Result>("personasPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Nacionalidad = x.nombre
                            };
                    break;
                case "5"://Tipo de vinculación
                    query = from x in db.Database
                    .SqlQuery<personasPersonas_dimension_años_Result>("personasPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Tipo_Vinculación = x.nombre
                            };
                    break;
                default://Sexo
                    query = from x in db.Database
                    .SqlQuery<personasPersonas_dimension_años_Result>("personasPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Sexo = x.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PersonasPersonas/GraficoxYear/tabla/desde/hasta
        //lista todos los PersonasPersonas segun el tipo de tabla, el año (sin periodo) desde y hasta
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from x in db.Database
                    .SqlQuery<personasPersonas_dimension_años_Result>("personasPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby x.año, x.nombre
                        select new
                        {
                            x.año,
                            x.total,
                            x.totalTE,
                            x.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PersonasPersonas/PersonasPersonasYearSexo/yearSearch
        //Lista PersonasPersonas de un año por sexo
        public JsonResult PersonasPersonasYearSexo(string yearSearch)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);

            var query = from x in db.Database
                .SqlQuery<personasPersonas_sexo_Result>("personasPersonas_sexo @year", yearParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PersonasPersonas/PersonasPersonasYearEdad/yearSearch
        //Lista PersonasPersonas de un año por edad
        public JsonResult PersonasPersonasYearEdad(string yearSearch)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);

            var query = from x in db.Database
                .SqlQuery<personasPersonas_edad_Result>("personasPersonas_edad @year", yearParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PersonasPersonas/PersonasPersonasYearFormacion/yearSearch
        //Lista PersonasPersonas de un año por nivel de formación
        public JsonResult PersonasPersonasYearFormacion(string yearSearch)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);

            var query = from x in db.Database
                .SqlQuery<personasPersonas_formacion_Result>("personasPersonas_formacion @year", yearParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PersonasPersonas/PersonasPersonasYearNacionalidad/yearSearch
        //Lista PersonasPersonas de un año por nacionalidad
        public JsonResult PersonasPersonasYearNacionalidad(string yearSearch)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);

            var query = from x in db.Database
                .SqlQuery<personasPersonas_nacionalidad_Result>("personasPersonas_nacionalidad @year", yearParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PersonasPersonas/PersonasPersonasYearVinculacion/yearSearch
        //Lista PersonasPersonas de un año por tipo vinculación
        public JsonResult PersonasPersonasYearVinculacion(string yearSearch)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);

            var query = from x in db.Database
                .SqlQuery<personasPersonas_vinculacion_Result>("personasPersonas_vinculacion @year", yearParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /PersonasPersonas/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de PersonasPersonas por sexo
                    path = Path.Combine(Server.MapPath("~/Reportes/PersonasPersonas"), "PersonasPersonas_sexo.rdlc");
                    break;
                case "2": //reporte de PersonasPersonas por rango de edad
                    path = Path.Combine(Server.MapPath("~/Reportes/PersonasPersonas"), "PersonasPersonas_rango_edad.rdlc");
                    break;
                case "3": //reporte de PersonasPersonas por nivel de formación
                    path = Path.Combine(Server.MapPath("~/Reportes/PersonasPersonas"), "PersonasPersonas_nivel_formación.rdlc");
                    break;
                case "4": //reporte de PersonasPersonas por nacionalidad
                    path = Path.Combine(Server.MapPath("~/Reportes/PersonasPersonas"), "PersonasPersonas_nacionalidad.rdlc");
                    break;
                case "5": //reporte de PersonasPersonas por tipo de vinculación
                    path = Path.Combine(Server.MapPath("~/Reportes/PersonasPersonas"), "PersonasPersonas_tipo_vinculación.rdlc");
                    break;
                default: //reporte de PersonasPersonas completo
                    path = Path.Combine(Server.MapPath("~/Reportes/PersonasPersonas"), "PersonasPersonas.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from x in db.Lista_PersonasPersonas
                        select new
                        {
                            x.Año,
                            x.Periodo,
                            x.Vinculo,
                            x.Formacion,
                            x.Nacionalidad,
                            x.Edad,
                            x.Sexo,
                            x.Cantidad,
                            x.CantidadTE
                        };
            }
            else
            {
                query = from x in db.Lista_PersonasPersonas.ToList()
                        where
                           x.Año.ToString().CompareTo(desde) >= 0 &&
                           x.Año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            x.Año,
                            x.Periodo,
                            x.Vinculo,
                            x.Formacion,
                            x.Nacionalidad,
                            x.Edad,
                            x.Sexo,
                            x.Cantidad,
                            x.CantidadTE
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}