﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class AreaEspaciosController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 88675309 // Usa este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /AreaEspacios/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /AreaEspacios/getYears
        //lista los años de registros de las áreas de los espacios utilizados para acti
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_Espacios
                         orderby year.año
                         select new
                         {
                             año = year.año
                         })
                         .ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /AreaEspacios/Listar
        //lista todas las áreas de los espacios utilizados para acti
        public JsonResult Listar()
        {
            var query = from e in db.Lista_Espacios
                        select new
                        {
                            Disciplina_OCDE = e.Disciplina,
                            Subárea_OCDE = e.Subarea,
                            Área_OCDE = e.Area,
                            Año = e.año,
                            Periodo = e.Periodo,
                            ACTI_N3 = e.ACTI_N3,
                            ACTI_N2 = e.ACTI_N2,
                            ACTI_N1 = e.ACTI,
                            Ubicación_Geográfica = e.UbicacionGeo,
                            Cantidad = e.Cantidad
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /AreaEspacios/ListarxYear/tabla/desde/hasta
        //lista todas las áreas de los espacios utilizados para acti segun el tipo de tabla, el año (con periodo) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2"://Subárea OCDE
                    query = from e in db.Database
                    .SqlQuery<espacios_dimension_años_Result>("espacios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.nombre
                            select new
                            {
                                Año = e.año,
                                Cantidad = e.total,
                                Subárea_OCDE = e.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from e in db.Database
                    .SqlQuery<espacios_dimension_años_Result>("espacios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.nombre
                            select new
                            {
                                Año = e.año,
                                Cantidad = e.total,
                                Disciplina_OCDE = e.nombre
                            };
                    break;
                case "4"://ACTI nivel 1
                    query = from e in db.Database
                    .SqlQuery<espacios_dimension_años_Result>("espacios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.nombre
                            select new
                            {
                                Año = e.año,
                                Cantidad = e.total,
                                ACTI_N1 = e.nombre
                            };
                    break;
                case "5"://ACTI nivel 2
                    query = from e in db.Database
                    .SqlQuery<espacios_dimension_años_Result>("espacios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.nombre
                            select new
                            {
                                Año = e.año,
                                Cantidad = e.total,
                                ACTI_N2 = e.nombre
                            };
                    break;
                case "6"://ACTI nivel 3
                    query = from e in db.Database
                    .SqlQuery<espacios_dimension_años_Result>("espacios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.nombre
                            select new
                            {
                                Año = e.año,
                                Cantidad = e.total,
                                ACTI_N3 = e.nombre
                            };
                    break;
                case "7"://Ubicación geográfica
                    query = from e in db.Database
                    .SqlQuery<espacios_dimension_años_Result>("espacios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.nombre
                            select new
                            {
                                Año = e.año,
                                Cantidad = e.total,
                                Ubicación_Geográfica = e.nombre
                            };
                    break;
                default://Área ocde
                    query = from e in db.Database
                    .SqlQuery<espacios_dimension_años_Result>("espacios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby e.año, e.nombre
                            select new
                            {
                                Año = e.año,
                                Cantidad = e.total,
                                Área_OCDE = e.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /AreaEspacios/GraficoxYear/tabla/desde/hasta
        //lista todas las áreas de los espacios utilizados para acti segun el tipo de tabla, el año (sin periodo) desde y hasta.
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from e in db.Database
                    .SqlQuery<espacios_dimension_años_Result>("espacios_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby e.año, e.nombre
                        select new
                        {
                            e.año,
                            e.total,
                            e.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /AreaEspacios/EspaciosYearUbicacionOCDE/yearSearch/catOCDE
        //Lista las áreas de los espacios utilizados para acti de un año por nivel OCDE y ubicación geográfica
        public JsonResult EspaciosYearUbicacionOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from e in db.Database
                .SqlQuery<espacios_ocde_ubicacionGeo_Result>("espacios_ocde_ubicacionGeo @year, @ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby e.nombre, e.ubicacion
                        select new
                        {
                            e.total,
                            e.nombre,
                            e.ubicacion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /AreaEspacios/EspaciosYearUbicacionACTI/yearSearch/catACTI
        //Lista las áreas de los espacios utilizados para acti de un año por nivel ACTI y ubicación geográfica
        public JsonResult EspaciosYearUbicacionACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from e in db.Database
                .SqlQuery<espacios_acti_ubicacionGeo_Result>("espacios_acti_ubicacionGeo @year, @acti", yearParameter, actiParameter)
                .ToList()
                        orderby e.nombre, e.ubicacion
                        select new
                        {
                            e.total,
                            e.nombre,
                            e.ubicacion
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /AreaEspacios/EspaciosYearACTIOCDE/yearSearch/catOCDE/catACTI
        //Lista las áreas de los espacios utilizados para acti de un año por nivel OCDE y ACTI
        public JsonResult EspaciosYearACTIOCDE(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from e in db.Database
                .SqlQuery<espacios_ocde_acti_Result>("espacios_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby e.nombre, e.acti
                        select new
                        {
                            e.total,
                            e.nombre,
                            e.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /AreaEspacios/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de AreaEspacios por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/AreaEspacios"), "ÁreaEspacios_área_ocde.rdlc");
                    break;
                case "2": //reporte de AreaEspacios por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/AreaEspacios"), "ÁreaEspacios_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de AreaEspacios por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/AreaEspacios"), "ÁreaEspacios_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de AreaEspacios por acti nivel 1
                    path = Path.Combine(Server.MapPath("~/Reportes/AreaEspacios"), "ÁreaEspacios_acti_n1.rdlc");
                    break;
                case "5": //reporte de AreaEspacios por acti nivel 2
                    path = Path.Combine(Server.MapPath("~/Reportes/AreaEspacios"), "ÁreaEspacios_acti_n2.rdlc");
                    break;
                case "6": //reporte de AreaEspacios por acti nivel 3
                    path = Path.Combine(Server.MapPath("~/Reportes/AreaEspacios"), "ÁreaEspacios_acti_n3.rdlc");
                    break;
                case "7": //reporte de AreaEspacios por ubicación geográfica
                    path = Path.Combine(Server.MapPath("~/Reportes/AreaEspacios"), "ÁreaEspacios_ubicación_geográfica.rdlc");
                    break;
                default: //reporte de AreaEspacios completo
                    path = Path.Combine(Server.MapPath("~/Reportes/AreaEspacios"), "ÁreaEspacios.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from e in db.Lista_Espacios
                        select new
                        {
                            Disciplina = e.Disciplina,
                            Subarea = e.Subarea,
                            Area = e.Area,
                            Año = e.año,
                            Periodo = e.Periodo,
                            ACTI_N3 = e.ACTI_N3,
                            ACTI_N2 = e.ACTI_N2,
                            ACTI_N1 = e.ACTI,
                            Ubicacion = e.UbicacionGeo,
                            Espacios = e.Cantidad
                        };
            }
            else
            {
                query = from e in db.Lista_Espacios.ToList()
                        where
                           e.año.ToString().CompareTo(desde) >= 0 &&
                           e.año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            Disciplina = e.Disciplina,
                            Subarea = e.Subarea,
                            Area = e.Area,
                            Año = e.año,
                            Periodo = e.Periodo,
                            ACTI_N3 = e.ACTI_N3,
                            ACTI_N2 = e.ACTI_N2,
                            ACTI_N1 = e.ACTI,
                            Ubicacion = e.UbicacionGeo,
                            Espacios = e.Cantidad
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }

    }
}