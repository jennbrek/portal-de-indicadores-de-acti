﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data.SqlClient;
using Portal_Indicadores_ACTI.Models;

namespace Portal_Indicadores_ACTI.Controllers
{
    public class GastosPersonasController : Controller
    {
        private dw_bdEntities db = new dw_bdEntities();

        // Este ActionResult sobreescribe JsonResult para cambiar la longitud máxima del JSON
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = 8675309 // Use este valor para modificar el modificar el tamaño de todos los Requests
            };
        }

        // GET: /GastosPersonas/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /GastosPersonas/getYears
        //lista los años de los registros de GastosPersonas
        public JsonResult getYears()
        {
            var query = (from year in db.Lista_GastosPersonas
                         orderby year.Año
                         select new
                         {
                             año = year.Año
                         }).ToList().Distinct();
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPersonas/Listar
        //lista todos los GastosPersonas
        public JsonResult Listar()
        {
            var query = from x in db.Lista_GastosPersonas
                        select new
                        {
                            ACTI_N1 = x.ACTI_N1,
                            ACTI_N2 = x.ACTI_N2,
                            ACTI_N3 = x.ACTI_N3,
                            Disciplina_OCDE = x.Disciplina,
                            Año = x.Año,
                            Periodo = x.Periodo,
                            Subárea_OCDE = x.Subarea,
                            Área_OCDE = x.Area,
                            Cantidad = x.Cantidad,
                            CantidadTE = x.CantidadTE
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPersonas/ListarxYear/tabla/desde/hasta
        //lista todos los GastosPersonas segun el tipo de tabla, el año (con periodo) desde y hasta
        public JsonResult ListarxYear(string tabla, string desde, string hasta)
        {
            var query = new System.Object();
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            switch (tabla)
            {
                case "2": //Subárea OCDE
                    query = from x in db.Database
                    .SqlQuery<gastosPersonas_dimension_años_Result>("gastosPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Subárea_OCDE = x.nombre
                            };
                    break;
                case "3"://Disciplina OCDE
                    query = from x in db.Database
                    .SqlQuery<gastosPersonas_dimension_años_Result>("gastosPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Disciplina_OCDE = x.nombre
                            };
                    break;
                case "4"://acti primer nivel
                    query = from x in db.Database
                    .SqlQuery<gastosPersonas_dimension_años_Result>("gastosPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                ACTI_N1 = x.nombre
                            };
                    break;
                case "5"://acti segundo nivel
                    query = from x in db.Database
                    .SqlQuery<gastosPersonas_dimension_años_Result>("gastosPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                ACTI_N2 = x.nombre
                            };
                    break;
                case "6"://acti tercer nivel
                    query = from x in db.Database
                    .SqlQuery<gastosPersonas_dimension_años_Result>("gastosPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                ACTI_N3 = x.nombre
                            };
                    break;
                default://Área OCDE
                    query = from x in db.Database
                    .SqlQuery<gastosPersonas_dimension_años_Result>("gastosPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                            orderby x.año, x.nombre
                            select new
                            {
                                Año = x.año,
                                Cantidad = x.total,
                                CantidadTE = x.totalTE,
                                Área_OCDE = x.nombre
                            };
                    break;
            }
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPersonas/GraficoxYear/tabla/desde/hasta
        //lista todos los GastosPersonas segun el tipo de tabla, el año (sin periodo) desde y hasta
        public JsonResult GraficoxYear(string tabla, string desde, string hasta)
        {
            var yearDesdeParameter = new SqlParameter("@yearDesde", desde);
            var yearHastaParameter = new SqlParameter("@yearHasta", hasta);
            var dimensionParameter = new SqlParameter("@dimension", tabla);
            var query = from x in db.Database
                    .SqlQuery<gastosPersonas_dimension_años_Result>("gastosPersonas_dimension_años @yearDesde,@yearHasta,@dimension", yearDesdeParameter, yearHastaParameter, dimensionParameter)
                    .ToList()
                        orderby x.año, x.nombre
                        select new
                        {
                            x.año,
                            x.total,
                            x.totalTE,
                            x.nombre
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPersonas/GastosPersonasOCDEACTI/yearSearch/catOCDE/catACTI
        //Lista los GastosPersonas para ACTI en un año por categoria ocde y nivel acti
        public JsonResult GastosPersonasOCDEACTI(string yearSearch, string catOCDE, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from x in db.Database
                .SqlQuery<gastosPersonas_ocde_acti_Result>("gastosPersonas_ocde_acti @year, @ocde, @acti", yearParameter, ocdeParameter, actiParameter)
                .ToList()
                        orderby x.nombre, x.acti
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre,
                            x.acti
                        };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPersonas/GastosPersonasYearACTI/yearSearch/catACTI
        //Lista GastosPersonas de un año por el nivel acti
        public JsonResult GastosPersonasYearACTI(string yearSearch, string catACTI)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var actiParameter = new SqlParameter("@acti", catACTI);

            var query = from x in db.Database
                .SqlQuery<gastosPersonas_acti_Result>("gastosPersonas_acti @year,@acti", yearParameter, actiParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPersonas/GastosPersonasYearOCDE/yearSearch/catOCDE
        //Lista GastosPersonas de un año por la clasificacion ocde
        public JsonResult GastosPersonasYearOCDE(string yearSearch, string catOCDE)
        {
            var yearParameter = new SqlParameter("@year", yearSearch);
            var ocdeParameter = new SqlParameter("@ocde", catOCDE);

            var query = from x in db.Database
                .SqlQuery<gastosPersonas_ocde_Result>("gastosPersonas_ocde @year,@ocde", yearParameter, ocdeParameter)
                .ToList()
                        orderby x.nombre
                        select new
                        {
                            x.total,
                            x.totalTE,
                            x.nombre
                        };

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        // GET: /GastosPersonas/Reporte/id/desde/hasta/tipoReporte/fileName
        //Retorna el reporte del indicador
        public ActionResult Reporte(string id, string desde, string hasta, string tipoReporte, string fileName)
        {
            LocalReport lr = new LocalReport();
            string path;
            switch (tipoReporte)
            {
                case "1": //reporte de GastosPersonas por área ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPersonas"), "GastosPersonas_área_ocde.rdlc");
                    break;
                case "2": //reporte de GastosPersonas por subárea ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPersonas"), "GastosPersonas_subárea_ocde.rdlc");
                    break;
                case "3": //reporte de GastosPersonas por disciplina ocde
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPersonas"), "GastosPersonas_disciplina_ocde.rdlc");
                    break;
                case "4": //reporte de GastosPersonas por acti primer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPersonas"), "GastosPersonas_acti_n1.rdlc");
                    break;
                case "5": //reporte de GastosPersonas por acti segundo nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPersonas"), "GastosPersonas_acti_n2.rdlc");
                    break;
                case "6": //reporte de GastosPersonas por acti tercer nivel
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPersonas"), "GastosPersonas_acti_n3.rdlc");
                    break;
                default: //reporte de GastosPersonas completo
                    path = Path.Combine(Server.MapPath("~/Reportes/GastosPersonas"), "GastosPersonas.rdlc");
                    break;
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
                lr.DisplayName = fileName;
            }
            else
            {
                return View("Index");
            }

            var query = new Object();
            if (desde.Equals("-100") || hasta.Equals("-100") || desde == null || hasta == null) //reporte por todos los años
            {
                query = from x in db.Lista_GastosPersonas
                        select new
                        {
                            x.Año,
                            x.Periodo,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            x.Cantidad,
                            x.CantidadTE
                        };
            }
            else
            {
                query = from x in db.Lista_GastosPersonas.ToList()
                        where
                           x.Año.ToString().CompareTo(desde) >= 0 &&
                           x.Año.ToString().CompareTo(hasta) <= 0
                        select new
                        {
                            x.Año,
                            x.Periodo,
                            x.Area,
                            x.Subarea,
                            x.Disciplina,
                            x.ACTI_N1,
                            x.ACTI_N2,
                            x.ACTI_N3,
                            x.Cantidad,
                            x.CantidadTE
                        };
            }

            ReportDataSource rd = new ReportDataSource("DS", query);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>12in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.75in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.75in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}