//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Portal_Indicadores_ACTI.Models
{
    using System;
    
    public partial class programas_ocde_acreditacion_Result
    {
        public int año { get; set; }
        public string nombre { get; set; }
        public string acreditacion { get; set; }
        public Nullable<int> total { get; set; }
    }
}
